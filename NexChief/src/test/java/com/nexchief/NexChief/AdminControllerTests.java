package com.nexchief.NexChief;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import com.nexchief.NexChief.entity.Admin;
import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.service.AdminService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class AdminControllerTests {

	@MockBean
	AdminService adminService;

	@Autowired
	MockMvc mockMvc;
	
	
	Admin loginCredentials = new Admin("admin", "admin");
	Admin loginFalseCredentials = new Admin("falseus", "admin");
	
	@Test
	public void adminLogin_success() throws Exception{
		when(adminService.adminLogin(loginCredentials)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Login Success")));
		String requestBody = "{\r\n"
				+ "    \"username\": \"admin\",\r\n"
				+ "    \"password\": \"admin\"\r\n"
				+ "	}\r\n";
		mockMvc.perform(post("/api/auth/login")
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print()).andExpect(status().isOk());	
	}
	
	
}
