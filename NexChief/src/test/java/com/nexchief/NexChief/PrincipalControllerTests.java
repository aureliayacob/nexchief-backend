package com.nexchief.NexChief;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import com.nexchief.NexChief.customfields.CustomPrincipal;
import com.nexchief.NexChief.dto.PrincipalDto;
import com.nexchief.NexChief.entity.Person;
import com.nexchief.NexChief.entity.Principal;
import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.service.PrincipalService;
import com.nexchief.NexChief.utils.DateFormatter;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class PrincipalControllerTests {

	@MockBean
	PrincipalService principalService;

	@Autowired
	MockMvc mockMvc;

	@MockBean
	DateFormatter formatter;

	CustomPrincipal.GetFullDataPrincipal principal = new CustomPrincipal.GetFullDataPrincipal() {

		@Override
		public String getprincipalId() {
			// TODO Auto-generated method stub
			return "LAL12";
		}

		@Override
		public String getUpdatedBy() {
			// TODO Auto-generated method stub
			return "ADMIN";
		}

		@Override
		public Timestamp getUpdatedAt() {
			// TODO Auto-generated method stub
			return Timestamp.valueOf("2022-01-07 11:43:28.000000");
		}

		@Override
		public String getPhone() {
			// TODO Auto-generated method stub
			return "081234654765";
		}

		@Override
		public Integer getPersonId() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return "Lalisa";
		}

		@Override
		public Date getLicensedExpiryDate() {
			// TODO Auto-generated method stub
			return new Date();
		}

		@Override
		public int getId() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public String getFax() {
			// TODO Auto-generated method stub
			return "02112345";
		}

		@Override
		public String getEmail() {
			// TODO Auto-generated method stub
			return "lalisa@gmail.com";
		}

		@Override
		public String getDeletedBy() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Timestamp getDeletedAt() {
			// TODO Auto-generated method stub
			return Timestamp.valueOf("2022-01-07 11:43:28.000000");
		}

		@Override
		public String getCreatedBy() {
			// TODO Auto-generated method stub
			return "ADMIN";
		}

		@Override
		public Timestamp getCreatedAt() {
			// TODO Auto-generated method stub
			return  Timestamp.valueOf("2022-01-07 11:43:28.000000");
		}

		@Override
		public String getCountry() {
			// TODO Auto-generated method stub
			return "INDONESIA";
		}

		@Override
		public String getCity() {
			// TODO Auto-generated method stub
			return "YOGYAKARTA";
		}

		@Override
		public String getAddress() {
			// TODO Auto-generated method stub
			return "JL PAINGAN MAGUWOHARJO";
		}
	};

	@Test
	public void addPrincipal() throws Exception{
		Person newPerson = new Person ("Lalisa", "Jl Paingan No.9 Maguwoharjo", "089603450835", "lalisam@gmail.com", "ADMIN", "ADMIN");
		Principal savePrincipal = new Principal("LAL12", "Yogyakarta", "02134576", "INDONESIA", formatter.formatStringToDate("2021-03-03"), newPerson);
		PrincipalDto newPrincipal = new PrincipalDto("LAL12", "Yogyakarta", "02134576", "INDONESIA", formatter.formatStringToDate("2021-03-03"),"Lalisa", 
				"Jl Paingan No.9 Maguwoharjo", "089603450835", "lalisam@gmail.com", "ADMIN", "ADMIN"); 

		ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "New Data Added")) ;

		when(principalService.addPrincipal(newPrincipal)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "New Data Added", savePrincipal)));
		String requestBody = "{\r\n"
				+ "    \"name\": \"Lalisa\",\r\n"
				+ "    \"address\": \"Jl Paingan No.9 Maguwoharjo\",\r\n"
				+ "    \"city\": \"Yogyakarta\",\r\n"
				+ "    \"email\": \"lalisam@gmail.com\",\r\n"
				+ "    \"phone\": \"089603450835\",\r\n"
				+ "    \"fax\": \"02134576\",\r\n"
				+ "    \"country\": \"INDONESIA\",\r\n"
				+ "    \"principalId\": \"LAL12\",\r\n"
				+ "		\"licensedExpiryDate\":\"2021-03-03\",\r\n"
				+ "		  \"createdBy\": \"ADMIN\",\r\n"
				+ "		  \"updatedBy\": \"ADMIN\"\r\n"
				+ "	}\r\n"
				+ "}";
		this.mockMvc.perform(post("/api/principal/add")
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print()).andExpect(status().isOk());	

	}

	@Test
	public void countPrincipals() throws Exception{
		Person newPerson = new Person ("Lalisa", "Jl Paingan No.9 Maguwoharjo", "089603450835", "lalisam@gmail.com", "ADMIN", "ADMIN");
		Principal savePrincipal = new Principal("LAL12", "Yogyakarta", "02134576", "INDONESIA", formatter.formatStringToDate("2021-03-03"), newPerson);

		List<Principal> principals = new ArrayList<>();
		principals.add(savePrincipal);

		Response res = new Response(HttpStatus.OK.value(),  "Counted", principals.size());
		when(principalService.countActivePrincipal()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		this.mockMvc.perform(get("/api/principals/count")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print()).andExpect(status().isOk());		
	}

	@Test
	public void getPrincipalByPrincipalIdTest() throws Exception{
		CustomPrincipal.GetFullDataPrincipal getPrincipal = principal;
		Response res = new Response(HttpStatus.OK.value(),  "Counted", getPrincipal);
		when(principalService.getPrincipalByPrincipalId("LAL12")).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		this.mockMvc.perform(get("/api/principal?id=LAL12").content(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());		
	}


	@Test
	public void getAllPrincipal() throws Exception {
		List<CustomPrincipal.GetFullDataPrincipal> allPrincipals = new ArrayList<>();
		allPrincipals.add(principal);
		Response res = new Response(HttpStatus.OK.value(),  "Counted", allPrincipals);
		when(principalService.getAllPrincipal()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		this.mockMvc.perform(get("/api/principal/all").content(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
	}

	@Test
	public void getAllPrincipalPage() throws Exception {
		List<CustomPrincipal.GetFullDataPrincipal> allPrincipals = new ArrayList<>();
		allPrincipals.add(principal);
		Response res = new Response(HttpStatus.OK.value(),  "Counted", allPrincipals);
		when(principalService.getAllPrincipalPage(7,0)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		this.mockMvc.perform(get("/api/principal/7/0").content(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
	}


	@Test
	public void searchPrincipal_found() throws Exception {
		List<CustomPrincipal.GetFullDataPrincipal> allPrincipals = new ArrayList<>();
		allPrincipals.add(principal);
		Response res = new Response(HttpStatus.OK.value(),  "Data Found", allPrincipals);
		when(principalService.searchPrincipalByName("La")).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		this.mockMvc.perform(get("/api/principal/search?name=La")
				.content(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void searchPrincipal_not_found() throws Exception {
		List<CustomPrincipal.GetFullDataPrincipal> allPrincipals = new ArrayList<>();
		allPrincipals.add(principal);
		Response res = new Response(HttpStatus.NOT_FOUND.value(),  "Data Not Found", allPrincipals);
		when(principalService.searchPrincipalByName("Ga")).thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(res));
		this.mockMvc.perform(get("/api/principal/search?name=Ga").content(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound());
	}

	@Test
	public void deletePrincipal_success() throws Exception {
		Response res = new Response(HttpStatus.OK.value(), "Principal Deleted");
		when(principalService.deletePrincipal(1)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		this.mockMvc.perform(delete("/api/principal/delete/1").content(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
	}

	@Test
	public void deletePrincipal_failed() throws Exception {
		Response res = new Response(HttpStatus.BAD_REQUEST.value(), "Deleting Failed");
		when(principalService.deletePrincipal(9)).thenReturn(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(res));
		this.mockMvc.perform(delete("/api/principal/delete/9").content(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isBadRequest());
	}


	@Test
	public void editPrincipal_success() throws Exception {
		PrincipalDto editPrincipal = new PrincipalDto("LAL12", "Yogyakarta", "02134576", "INDONESIA", formatter.formatStringToDate("2021-03-03"),"Lalisa Edited", 
				"Jl Paingan No.9 Maguwoharjo", "089603450835", "lalisam@gmail.com", "ADMIN", "ADMIN"); 

		Response res = new Response(HttpStatus.OK.value(), "Principal Updated");
		when(principalService.editPrincipal(1, editPrincipal)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		String requestBody = "{\r\n"
				+ "    \"name\": \"Lalisa Edited\",\r\n"
				+ "    \"address\": \"Jl Paingan No.9 Maguwoharjo\",\r\n"
				+ "    \"city\": \"Yogyakarta\",\r\n"
				+ "    \"email\": \"lalisam@gmail.com\",\r\n"
				+ "    \"phone\": \"089603450835\",\r\n"
				+ "    \"fax\": \"02134576\",\r\n"
				+ "    \"country\": \"INDONESIA\",\r\n"
				+ "	   \"licensedExpiryDate\":\"2021-03-03\",\r\n"
				+ "	   \"updatedBy\": \"ADMIN\"\r\n"
				+ "	}\r\n"
				+ "}";
		this.mockMvc.perform(put("/api/principal/edit/1")
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}


}

