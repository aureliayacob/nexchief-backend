package com.nexchief.NexChief;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import com.nexchief.NexChief.customfields.CustomUser;
import com.nexchief.NexChief.dto.UserCreateUpdateDto;
import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.service.UserService;
import com.nexchief.NexChief.utils.DateFormatter;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class UserControllerTests {
	@MockBean
	UserService userService;

	@Autowired
	MockMvc mockMvc;

	@MockBean
	DateFormatter formatter;

	CustomUser.GetFullDataUser dataUser = new CustomUser.GetFullDataUser() {

		@Override
		public Timestamp getUserValidThru() {
			// TODO Auto-generated method stub
			return Timestamp.valueOf("2022-06-07 11:43:28.000000");
		}

		@Override
		public String getUserId() {
			// TODO Auto-generated method stub
			return "JEJEBU";
		}

		@Override
		public String getUpdatedBy() {
			// TODO Auto-generated method stub
			return "ADMIN";
		}

		@Override
		public Timestamp getUpdatedAt() {
			// TODO Auto-generated method stub
			return  Timestamp.valueOf("2022-01-10 11:43:28.000000");
		}

		@Override
		public String getStatus() {
			// TODO Auto-generated method stub
			return "Regular";
		}

		@Override
		public Timestamp getRegistrationDate() {
			// TODO Auto-generated method stub
			return  Timestamp.valueOf("2022-01-10 11:43:28.000000");
		}

		@Override
		public Integer getPrincipalId() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public String getPhone() {
			// TODO Auto-generated method stub
			return "081603450835";
		}

		@Override
		public Integer getPersonId() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public String getPassword() {
			// TODO Auto-generated method stub
			return "Testpass999";
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return "Jeje Bulan";
		}

		@Override
		public int getId() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public String getEmail() {
			// TODO Auto-generated method stub
			return "jejebulan@gmail.com";
		}

		@Override
		public Integer getDistributorId() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public boolean getDisableLogin() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public String getDeletedBy() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Timestamp getDeletedAt() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getCreatedBy() {
			// TODO Auto-generated method stub
			return "ADMIN";
		}

		@Override
		public Timestamp getCreatedAt() {
			// TODO Auto-generated method stub
			return Timestamp.valueOf("2022-01-09 11:43:28.000000");
		}

		@Override
		public String getAddress() {
			// TODO Auto-generated method stub
			return "JL SULTAN AGUNG NO.9";
		}
	};

	@Test
	public void getAllUserPageTest() throws Exception {
		List<CustomUser.GetFullDataUser> users = new ArrayList<>();
		users.add(dataUser);
		Response res = new Response(HttpStatus.OK.value(),  "Data Found", users);

		when(userService.getAllUserPage(7, 0)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		mockMvc.perform(get("/api/user/7/0")
				.content(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void getAllUserTest() throws Exception {
		List<CustomUser.GetFullDataUser> users = new ArrayList<>();
		users.add(dataUser);
		Response res = new Response(HttpStatus.OK.value(),  "Data Found", users);

		when(userService.getAllUser()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		mockMvc.perform(get("/api/user/all")
				.content(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void getAllUserByIdTest() throws Exception {
		CustomUser.GetFullDataUser user = dataUser;
		Response res = new Response(HttpStatus.OK.value(),  "Data Found", user);

		when(userService.getUserById(1)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		mockMvc.perform(get("/api/user/1")
				.content(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void addUserTest() throws Exception {
		UserCreateUpdateDto newUser = new UserCreateUpdateDto("JEJEBU", "Jejebul999", "Regular", false,
				Timestamp.valueOf("2022-01-10 11:43:28.000000"), "Jeje Bulan", "Jl Mangkubumi No.9", 
				"081234567987", "jejebu@gmail.com", "ADMIN",
				"ADMIN", 1, 1);
		when(userService.addUser(newUser)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "New Data Added", newUser)));
		String requestBody = "{\r\n"
				+ "    \"disableLogin\": false,\r\n"
				+ "    \"password\": \"Jejebul999\",\r\n"
				+ "    \"status\": \"Regular\",\r\n"
				+ "    \"userId\": \"JEJEBU\",\r\n"
				+ "    \"userValidThru\": \"2022-01-10\",\r\n"
				+ "    \"distributorId\": 1,\r\n"
				+ "    \"principalId\": 1,\r\n"
				+ "    \"name\": \"Jeje Bulan\",\r\n"
				+ "    \"email\": \"jejebu@info.com\",\r\n"
				+ "    \"phone\": \"081234567987\",\r\n"
				+ "    \"address\": \"Jl Mangkubumi No.9\",\r\n"
				+ "	   \"createdBy\": \"ADMIN\",\r\n"
				+ "	   \"updatedBy\": \"ADMIN\"\r\n"
				+ "	}\r\n"
				+ "}";
		mockMvc.perform(post("/api/user/new")
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print()).andExpect(status().isOk());
	}
	
	@Test
	public void deleteUser_success() throws Exception {
		Response res = new Response(HttpStatus.OK.value(), "User Deleted");
		when(userService.hardDeleteUser(1)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		mockMvc.perform(delete("/api/user/delete/1")).andExpect(status().isOk());
	}
	
	@Test
	public void deleteUser_failed() throws Exception {
		Response res = new Response(HttpStatus.BAD_REQUEST.value(), "Deleting Failed");
		when(userService.hardDeleteUser(199)).thenReturn(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(res));
		mockMvc.perform(delete("/api/user/delete/199")).andExpect(status().isBadRequest());
	}
	
	@Test
	public void searchUser_found() throws Exception {
		List<CustomUser.GetFullDataUser> users = new ArrayList<>();
		users.add(dataUser);
		Response res = new Response(HttpStatus.OK.value(), "Data found");
		when(userService.searchUser("Bulan")).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		mockMvc.perform(get("/api/user/search?keyword=Bulan").contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}
	
	@Test
	public void searchUser_not_found() throws Exception {
		List<CustomUser.GetFullDataUser> users = new ArrayList<>();
		users.add(dataUser);
		Response res = new Response(HttpStatus.BAD_REQUEST.value(), "Data not found");
		when(userService.searchUser("Test")).thenReturn(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(res));
		mockMvc.perform(get("/api/user/search?keyword=Test").contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isBadRequest());
	}

	
	@Test
	public void editUserTest() throws Exception {
		UserCreateUpdateDto user = new UserCreateUpdateDto( "Jejebul999", "Premium", true,
				Timestamp.valueOf("2022-01-10 11:43:28.000000"), "Jeje Bulan", "Jl Mangkubumi No.9", 
				"081234567987", "jejebu@gmail.com", 
				"ADMIN", 1, 1);
		when(userService.editUser(1, user)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "New Data Added", user)));
		String requestBody = "{\r\n"
				+ "    \"disableLogin\": true,\r\n"
				+ "    \"password\": \"Jejebul999\",\r\n"
				+ "    \"status\": \"Premium\",\r\n"
				+ "    \"userValidThru\": \"2022-01-10\",\r\n"
				+ "    \"distributorId\": 1,\r\n"
				+ "    \"principalId\": 1,\r\n"
				+ "    \"name\": \"Jeje Bulan\",\r\n"
				+ "    \"email\": \"jejebu@info.com\",\r\n"
				+ "    \"phone\": \"081234567987\",\r\n"
				+ "    \"address\": \"Jl Mangkubumi No.9\",\r\n"
				+ "	   \"updatedBy\": \"ADMIN\"\r\n"
				+ "	}\r\n"
				+ "}";
		mockMvc.perform(put("/api/user/edit/1")
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print()).andExpect(status().isOk());
	}
		
	
	@Test
	public void countUserTest() throws Exception{
		List<CustomUser.GetFullDataUser> users = new ArrayList<>();
		users.add(dataUser);

		Response res = new Response(HttpStatus.OK.value(),  "Counted", users.size());
		when(userService.countUser()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		this.mockMvc.perform(get("/api/users/count")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print()).andExpect(status().isOk());		
	}
	
	
}
