package com.nexchief.NexChief;

import java.sql.Timestamp;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import com.nexchief.NexChief.customfields.CustomDistributor;
import com.nexchief.NexChief.customfields.CustomDistributor.GetFullDataDistributor;
import com.nexchief.NexChief.dto.DistributorCreateUpdateDto;
import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.service.DistributorService;
import com.nexchief.NexChief.utils.DateFormatter;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class DistributorControllerTests {

	@MockBean
	DistributorService distributorService;

	@Autowired
	MockMvc mockMvc;

	@MockBean
	DateFormatter formatter;

	CustomDistributor.GetFullDataDistributor dataDistributor = new CustomDistributor.GetFullDataDistributor() {

		@Override
		public String getownerLastName() {
			// TODO Auto-generated method stub
			return "Essense";
		}

		@Override
		public String getWebsite() {
			// TODO Auto-generated method stub
			return "ptessensegroup.com";
		}

		@Override
		public String getUpdatedBy() {
			// TODO Auto-generated method stub
			return "ADMIN";
		}

		@Override
		public Timestamp getUpdatedAt() {
			// TODO Auto-generated method stub
			return Timestamp.valueOf("2022-01-07 11:43:28.000000");
		}

		@Override
		public int getPrincipalId() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public String getPhone() {
			// TODO Auto-generated method stub
			return "081234567890";
		}

		@Override
		public String getOwnerFirstName() {
			// TODO Auto-generated method stub
			return "Lia";
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return "PT. Essense";
		}

		@Override
		public int getId() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public String getFax() {
			// TODO Auto-generated method stub
			return "02112345";
		}

		@Override
		public String getEmail() {
			// TODO Auto-generated method stub
			return "essense.info@gmail.com";
		}

		@Override
		public String getDistributorId() {
			// TODO Auto-generated method stub
			return "ESS12";
		}

		@Override
		public String getDeletedBy() {
			// TODO Auto-generated method stub
			return "ADMIN";
		}

		@Override
		public Timestamp getDeletedAt() {
			// TODO Auto-generated method stub
			return Timestamp.valueOf("2022-01-07 11:43:28.000000");
		}

		@Override
		public String getCreatedBy() {
			// TODO Auto-generated method stub
			return "ADMIN";
		}

		@Override
		public Timestamp getCreatedAt() {
			// TODO Auto-generated method stub
			return Timestamp.valueOf("2022-01-07 11:43:28.000000");
		}

		@Override
		public String getCountry() {
			// TODO Auto-generated method stub
			return "INDONESIA";
		}

		@Override
		public String getCity() {
			// TODO Auto-generated method stub
			return "YOGYAKARTA";
		}

		@Override
		public String getAddress() {
			// TODO Auto-generated method stub
			return "JL SULTAN AGUNG NO.12";
		}
	};

	@Test
	public void getAllDistributorPageTest() throws Exception{
		List<CustomDistributor.GetFullDataDistributor> distributors = new ArrayList<>(); ;
		distributors.add(dataDistributor);
		Response res = new Response(HttpStatus.OK.value(),  "Data Found", distributors);

		when(distributorService.getDistributorPage(7, 0)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));

		mockMvc.perform(get("/api/distributor/7/0")
				.content(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void getAllDistributorTest() throws Exception{
		List<CustomDistributor.GetFullDataDistributor> distributors = new ArrayList<>(); ;
		distributors.add(dataDistributor);
		Response res = new Response(HttpStatus.OK.value(),  "Data Found", distributors);

		when(distributorService.getAllDistributor()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));

		mockMvc.perform(get("/api/distributor/all")
				.content(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void getAllDistributorPerPrincipalTest() throws Exception{
		List<CustomDistributor.GetFullDataDistributor> distributors = new ArrayList<>(); ;
		distributors.add(dataDistributor);
		Response res = new Response(HttpStatus.OK.value(),  "Data Found", distributors);

		when(distributorService.getAllDistributorPerPrincipal(1)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));

		mockMvc.perform(get("/api/distributors/1")
				.content(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void getDistributorByDistributorIdTest_Found() throws Exception{
		CustomDistributor.GetFullDataDistributor distributor = dataDistributor; ;
		Response res = new Response(HttpStatus.OK.value(),  "Data Found", distributor);

		when(distributorService.getDistributorByDistributorId("ESS12")).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));

		mockMvc.perform(get("/api/distributor?id=ESS12")
				.content(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void getDistributorByDistributorIdTest_NotFound() throws Exception{
		CustomDistributor.GetFullDataDistributor distributor = dataDistributor; ;
		Response res = new Response(HttpStatus.NOT_FOUND.value(),  "Data Not Found", distributor);

		when(distributorService.getDistributorByDistributorId("TEST")).thenReturn(ResponseEntity.status(HttpStatus.NOT_FOUND).body(res));

		mockMvc.perform(get("/api/distributor?id=TEST")
				.content(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isNotFound());
	}

	@Test
	public void addDistributor() throws Exception {
		DistributorCreateUpdateDto distributor = new DistributorCreateUpdateDto("ESS12", "YOGYAKARTA", "Elisse", "Li",
				"02112345", "INDONESIA", "elisegroup.com", "PT Elisse Group", "Jl Paingan No.9 Maguwoharjo", "081234567876", "elisse@gmail.com",
				"ADMIN", "ADMIN", 1); 

		when(distributorService.addDistributor(distributor)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "New Data Added", distributor)));
		String requestBody = "{\r\n"
				+ "    \"city\": \"YOGYAKARTA\",\r\n"
				+ "    \"country\": \"INDONESIA\",\r\n"
				+ "    \"distributorId\": \"ESS12\",\r\n"
				+ "    \"fax\": \"02134576\",\r\n"
				+ "    \"ownerFirstName\": \"Elisse\",\r\n"
				+ "    \"ownerLastName\": \"Li\",\r\n"
				+ "    \"website\": \"elissegroup.com\",\r\n"
				+ "    \"principalId\": 1,\r\n"
				+ "    \"name\": \"PT Elisse Group\",\r\n"
				+ "    \"email\": \"elisse@info.com\",\r\n"
				+ "    \"phone\": \"089603450835\",\r\n"
				+ "    \"address\": \"Jl Paingan No.9 Maguwoharjo\",\r\n"
				+ "	   \"createdBy\": \"ADMIN\",\r\n"
				+ "	   \"updatedBy\": \"ADMIN\"\r\n"
				+ "	}\r\n"
				+ "}";
		mockMvc.perform(post("/api/distributor/new")
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
		.andDo(print()).andExpect(status().isOk());	
	}

	@Test
	public void deleteDistributor_success() throws Exception {
		Response res = new Response(HttpStatus.OK.value(), "Distributor Deleted");
		when(distributorService.deleteDistributor(1)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		mockMvc.perform(delete("/api/distributor/delete/1")).andExpect(status().isOk());
	}

	@Test
	public void deleteDistributor_failed() throws Exception {
		Response res = new Response(HttpStatus.BAD_REQUEST.value(), "Deleting Failed");
		when(distributorService.deleteDistributor(9)).thenReturn(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(res));
		mockMvc.perform(delete("/api/distributor/delete/9")).andExpect(status().isBadRequest());
	}

	@Test
	public void searchDistributor_found() throws Exception {
		List<CustomDistributor.GetFullDataDistributor> allDistributor = new ArrayList<>();
		allDistributor.add(dataDistributor);
		Response res = new Response(HttpStatus.OK.value(), "Data found");
		when(distributorService.searchDistributor("Ess")).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		mockMvc.perform(get("/api/distributor/search?name=Ess").contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void searchDistributor_not_found() throws Exception {
		List<CustomDistributor.GetFullDataDistributor> allDistributor = new ArrayList<>();
		allDistributor.add(dataDistributor);
		Response res = new Response(HttpStatus.BAD_REQUEST.value(), "Data not found");
		when(distributorService.searchDistributor("Ess")).thenReturn(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(res));
		mockMvc.perform(get("/api/distributor/search?name=Test").contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	public void editDistributor() throws Exception {
		DistributorCreateUpdateDto editDistributor = new DistributorCreateUpdateDto("YOGYAKARTA", "Anak Agung", "Gde", "02112345",
				"INDONESIA", "elissegroup.com", "PT Elisse Group", "Jl Mangkubumi no.5", "081234567890", "anakagunggde@gmail.com",
				1);
		Response res = new Response(HttpStatus.OK.value(), "Principal Updated");
		when(distributorService.editDistributor(1, editDistributor)).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
		String requestBody = "{\r\n"
				+ "    \"city\": \"YOGYAKARTA\",\r\n"
				+ "    \"country\": \"INDONESIA\",\r\n"
				+ "    \"distributorId\": \"ESS12\",\r\n"
				+ "    \"fax\": \"02134576\",\r\n"
				+ "    \"ownerFirstName\": \"Elisse\",\r\n"
				+ "    \"ownerLastName\": \"Li\",\r\n"
				+ "    \"website\": \"elissegroup.com\",\r\n"
				+ "    \"principalId\": 1,\r\n"
				+ "    \"name\": \"PT Elisse Group\",\r\n"
				+ "    \"email\": \"elisse@info.com\",\r\n"
				+ "    \"phone\": \"089603450835\",\r\n"
				+ "    \"address\": \"Jl Paingan No.9 Maguwoharjo\"\r\n"
				+ "	}\r\n"
				+ "}";
		mockMvc.perform(put("/api/distributor/edit/1").content(requestBody).contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}
		
		@Test
		public void countDistributorTest() throws Exception{

			List<GetFullDataDistributor> distributors = new ArrayList<>();
			distributors.add(dataDistributor);

			Response res = new Response(HttpStatus.OK.value(),  "Counted", distributors.size());
			when(distributorService.countDistributor()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(res));
			this.mockMvc.perform(get("/api/distributors/count")
					.contentType(MediaType.APPLICATION_JSON_VALUE))
			.andDo(print()).andExpect(status().isOk());		
		}
	}
	


