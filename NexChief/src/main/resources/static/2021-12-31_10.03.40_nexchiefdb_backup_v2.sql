-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: nexchief
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `nexchief`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `nexchief` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `nexchief`;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `person_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gfn44sntic2k93auag97juyij` (`username`),
  KEY `FKbbnoi2fayn2bitgaqvua4iyrv` (`person_id`),
  CONSTRAINT `FKbbnoi2fayn2bitgaqvua4iyrv` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'$2a$12$CF64.zP.cdnLBVTxlkk.Hus8KBz0Wl/Cd7bdhLisdNG5nTAHh20yO','ADMIN',1);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_record`
--

DROP TABLE IF EXISTS `db_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `db_record` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `last_update` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_record`
--

LOCK TABLES `db_record` WRITE;
/*!40000 ALTER TABLE `db_record` DISABLE KEYS */;
INSERT INTO `db_record` VALUES (1,'2021-12-28_13.50.08_nexchiefdb_backup_v1.sql','\"C:\\Users\\Nexsoft\\Documents\\Bootcamp\\NexChief\\Development\\NexChief\\backupdb\\2021-12-28_13.50.08_nexchiefdb_backup_v1.sql\"','2021-12-28 13:50:08.459000'),(2,'2021-12-28_15.59.47_nexchiefdb_backup_v2.sql','\"C:\\Users\\Nexsoft\\Documents\\Bootcamp\\NexChief\\Development\\NexChief\\backupdb\\2021-12-28_15.59.47_nexchiefdb_backup_v2.sql\"','2021-12-28 15:59:47.898000'),(3,'2021-12-29_14.05.58_nexchiefdb_backup_v1.sql','\"C:\\Users\\Nexsoft\\Documents\\Bootcamp\\NexChief\\Development\\NexChief\\backupdb\\2021-12-29_14.05.58_nexchiefdb_backup_v1.sql\"','2021-12-29 14:05:58.559000'),(4,'2021-12-29_14.06.10_nexchiefdb_backup_v2.sql','\"C:\\Users\\Nexsoft\\Documents\\Bootcamp\\NexChief\\Development\\NexChief\\backupdb\\2021-12-29_14.06.10_nexchiefdb_backup_v2.sql\"','2021-12-29 14:06:10.469000'),(5,'2021-12-30_15.00.28_nexchiefdb_backup_v1.sql','\"C:\\Users\\Nexsoft\\Documents\\Bootcamp\\NexChief\\Development\\NexChief\\backupdb\\2021-12-30_15.00.28_nexchiefdb_backup_v1.sql\"','2021-12-30 15:00:28.368000'),(6,'2021-12-31_09.42.36_nexchiefdb_backup_v1.sql','\"C:\\Users\\Nexsoft\\Documents\\Bootcamp\\NexChief\\Development\\NexChief\\backupdb\\2021-12-31_09.42.36_nexchiefdb_backup_v1.sql\"','2021-12-31 09:42:36.656000');
/*!40000 ALTER TABLE `db_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distributor`
--

DROP TABLE IF EXISTS `distributor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distributor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `distributor_id` varchar(255) NOT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `owner_first_name` varchar(255) NOT NULL,
  `owner_last_name` varchar(255) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `person_id` int DEFAULT NULL,
  `principal_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_s5rubftx127m45a12wnn8cq7h` (`distributor_id`),
  KEY `FKowh8m0hyouel7x4782ok1eg2q` (`person_id`),
  KEY `FKodovf5dkiktxlwl1ojlooy6yv` (`principal_id`),
  CONSTRAINT `FKodovf5dkiktxlwl1ojlooy6yv` FOREIGN KEY (`principal_id`) REFERENCES `principal` (`id`),
  CONSTRAINT `FKowh8m0hyouel7x4782ok1eg2q` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distributor`
--

LOCK TABLES `distributor` WRITE;
/*!40000 ALTER TABLE `distributor` DISABLE KEYS */;
INSERT INTO `distributor` VALUES (1,'Yogyakarta','Indonesia','POPG','(021)67598','Ymiru','Historia','historiavillage.com',4,3),(2,'Yogyakarta','Indonesia','SRIRJK','(021)764537','Sri','Larissa','srirejeki.co.id',5,2),(4,'Yogyakarta','Indonesia','SHAY66','(021)67598','Sasha','Braush','sashafarm.com',21,3),(5,'Yogyakarta','Indonesia','CHIT25','(021)87452','Chitoge','Krisaki','chitogegroup.com',26,3),(6,'Yogyakarta','Indonesia','COCO19','(021)87452','Chitoge','Krisaki','chitogegroup.com',27,3),(7,'Yogyakarta','Indonesia','HERO11','(021)87452','Ymir','Krisaki','herostore.com',50,3),(8,'Yogyakarta','Indonesia','ACKE93','(021)87452','Ymir','Krisaki','ackerman.com',53,5),(9,'YOGYAKARTA','INDONESIA','KETC19','01234532','KEKE','BUBUY','ketchupgroup.com',55,24),(10,'YOGYAKARTA','INDONESIA','JOJO53','(123)12354','JOJO','JIJI','jojo.com',59,25),(11,'TANGERANG','INDONESIA','SYSN51','','SYNISTER','GATES','sysntergroup.com',64,27),(12,'YOGYAKARTA','INDONESIA','LOLI13','021345786','BRIANA','CRY','lolicagroup.com',71,17),(13,'YOGYAKARTA','INDONESIA','BCMA','021345678','CECE','CICI','',72,16);
/*!40000 ALTER TABLE `distributor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `person` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_p0wr4vfyr2lyifm8avi67mqw5` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'Jl Newton Gading Serpong','2021-12-02 10:05:26.000000','Admin','admin@gmail.com','Cardi B','081657483756','2021-12-02 10:05:26.000000','Admin',NULL,NULL),(2,'Jl Pakualaman No 5','2021-12-02 10:13:49.000000','Admin','kongguan@gmail.com','KONG GUAN','081746574893','2021-12-02 10:13:49.000000','Admin',NULL,NULL),(3,'Jl Parangtritis No 28','2021-12-02 10:13:49.000000','Admin','sosro@gmail.com','SOSRO','089645372847','2021-12-02 10:13:49.000000','Admin',NULL,NULL),(4,'Jl Mangkubumi Baru No 1','2021-12-02 10:13:49.000000','Admin','historiagroup@gmail.com','HISTORIA VILLAGE','089546354746','2021-12-07 23:29:46.000000','Admin','Admin','2021-12-06 00:51:37.000000'),(5,'Jl Paingan Maguwoharjo','2021-12-02 10:13:49.000000','Admin','srirejeki@yahoo.com','SRI REJEKI','081576849304','2021-12-02 10:13:49.000000','Admin',NULL,'2021-12-05 21:33:38.000000'),(6,'Jl Jendral Sudirman','2021-12-02 10:13:49.000000','Admin','wijayatoko@yahoo.com','WIJAYA TOSERBA','081546354654','2021-12-02 10:13:49.000000','Admin',NULL,'2021-12-06 11:21:27.000000'),(7,'Jl Paingan 9 Maguwoharjo','2021-12-03 13:48:42.000000','Admin','aureliayacob@gmail.com','Aurelia Yacob','081356876345','2021-12-03 13:48:42.000000','Admin',NULL,'2021-12-20 02:29:23.000000'),(10,'Jl Pakualamos','2021-12-03 15:57:28.889000','Admin','ikhsanudin@gmail.com','OH SEHUN','084567354635','2021-12-20 01:16:56.000000',NULL,NULL,NULL),(19,'Graha Famili Blok 5','2021-12-05 20:13:30.370000','Admin','stellagroup@gmail.com','SLEEP WALKING','081546372847','2021-12-28 19:28:18.000000','Admin',NULL,NULL),(21,'Jl Demangan Baru No 5','2021-12-06 00:40:36.650000','Admin','shaday@gmail.com','SHAYS FARMDAY','081345756352','2021-12-06 00:40:36.650000','Admin',NULL,NULL),(24,'AAAAAAA','2021-12-06 11:15:15.948000','Admin','aaaa@gmail.com','FADER LILY','22222222','2021-12-30 14:11:22.000000','ADMIN',NULL,NULL),(25,'Jl Sultan Jaya','2021-12-06 14:39:48.035000','Admin','papercut.group@gmail.com','PAPER CUT','08123456789384','2021-12-06 14:39:48.035000','Admin',NULL,NULL),(26,'Jl Mrican Tromol','2021-12-06 14:49:20.543000','Admin','chitogestore@gmail.com','CHITOGE STORE','081345756352','2021-12-06 14:49:20.543000','Admin','Admin','2021-12-21 21:35:13.000000'),(27,'Jl Mrican Tromol','2021-12-07 00:19:51.775000','Admin','chitogestore@gmail.com','COCO CUCUC','081345756352','2021-12-07 00:19:51.775000','Admin',NULL,NULL),(30,'Jl Kaliuran Km 8','2021-12-07 23:41:52.604000','Admin','horizongroup@gmail.com','THE HORIZON','081374734756','2021-12-07 23:41:52.604000','Admin',NULL,NULL),(32,'Jl Kaliuran Km 8','2021-12-07 23:49:05.583000','Admin','horizongroup@gmail.com','ICE TEA ','081374734756','2021-12-29 13:39:43.000000','ADMIN',NULL,NULL),(34,'Jl Kaliuran Km 8','2021-12-07 23:51:38.554000','Admin','horizongroup@gmail.com','THE HORIIZIN','081374734756','2021-12-07 23:51:38.554000','Admin',NULL,NULL),(36,'Jl Sultan Jaya','2021-12-10 09:37:11.351000','Admin','yacob.group@gmail.com','YACOB GROUP','08123456789384','2021-12-29 11:27:00.000000','ADMIN','',NULL),(37,'JL SCIENTIA BOULEVARD, GADING SERPONG','2021-12-19 17:14:12.818000','ADMIN','bellalangroup@gmail.com','BELLA LANG','081234567890','2021-12-19 17:14:12.818000','ADMIN',NULL,'2021-12-30 09:40:58.000000'),(38,'JL SCIENTIA BOULEVARD, GADING SERPONG','2021-12-19 20:19:26.638000','ADMIN','madakonosekaiwa@gmail.com','MADA KONO SEKAI','081234567890','2021-12-19 20:19:26.638000','ADMIN',NULL,NULL),(39,'JL PAINGAN MAGUWOHARJO','2021-12-19 21:44:29.392000','ADMIN','annieleonhard@gmail.com','ANNIE LEONHARD','0812344567883','2021-12-19 21:44:29.392000','ADMIN',NULL,NULL),(40,'Jl Sultan Jaya','2021-12-19 21:50:13.835000','Admin','papercut.group@gmail.com','ASH GREY','08123456789384','2021-12-19 21:50:13.835000','Admin',NULL,NULL),(41,'AAAAAAAAAAAAAAAAAAA','2021-12-19 21:52:24.237000','ADMIN','aaaaaa@gmail.com','AKANAE','01234567890','2021-12-19 21:52:24.237000','ADMIN',NULL,NULL),(42,'JL BOULEVARD GADING','2021-12-19 23:55:49.254000','ADMIN','chrismasudesu@gmail.com','HISTORIA REISS','081234567890','2021-12-29 13:41:01.000000','ADMIN',NULL,'2021-12-29 13:55:04.000000'),(43,'XXXXXXXXXXX','2021-12-20 13:10:11.043000','ADMIN','xxxxxxx@gmail.com','TAKA HIROX','0812345567342','2021-12-20 13:15:40.000000',NULL,NULL,'2021-12-20 13:15:45.000000'),(50,'Jl Mrican Tromol','2021-12-20 14:46:31.891000','Admin','herostore@gmail.com','HERO WIN','081345756352','2021-12-20 14:46:31.891000','Admin',NULL,NULL),(51,'JL BOULEVARD GADING ','2021-12-20 17:02:39.236000','ADMIN','jennie@gmail.com','JENNIE KIM','081456765234','2021-12-20 17:02:39.236000','ADMIN',NULL,NULL),(53,'Jl Mrican Tromol','2021-12-21 04:30:01.845000','Admin','ackerman@gmail.com','ACKER MANDY','081345756352','2021-12-21 05:51:27.000000','ADMIN',NULL,NULL),(54,'JL BOULEVARD GADING SERPONG','2021-12-21 04:30:15.328000','Admin','giantgroup@gmail.com','GIANT DEPARTMENT STORE','081234567890','2021-12-21 04:30:15.328000','Admin',NULL,NULL),(55,'JL MOSES GATOT KACA, GEJAYAN','2021-12-21 04:35:17.919000','Admin','ketchupgroup@gmail.com','KETCHUP GROUP','081234567890','2021-12-21 05:50:48.000000','ADMIN','Admin','2021-12-21 06:08:56.000000'),(56,'JL PAINGAN, MAGUWOHARJO, SLEMAN','2021-12-23 10:16:02.597000','ADMIN','principaltest@gmail.com','KAMABOKO KONPACHIRO','081234567876','2021-12-28 19:40:46.000000','ADMIN',NULL,'2021-12-28 21:29:23.000000'),(58,'AAAAAAA','2021-12-23 13:28:54.845000','ADMIN','aaaaaa@gmail.com','CYNTYA BELLALANG','22222222','2021-12-28 20:01:12.000000','ADMIN',NULL,NULL),(59,'JL JOJOJOOJO','2021-12-23 13:45:33.813000','Admin','jojo@gmail.com','JOJO JOJOBI','081234567821','2021-12-30 09:22:56.000000','ADMIN',NULL,NULL),(60,'Jl Sultan Jaya','2021-12-28 19:28:34.061000','Admin','papercut.group@gmail.com','DOJA CHAN','08123456789384','2021-12-28 19:40:15.000000','ADMIN',NULL,NULL),(61,'JL SIMPANG 7, JAWA','2021-12-28 19:42:36.052000','ADMIN','nezukochan@gmail.com','NEZUKO CHAN','081234567876','2021-12-28 20:33:54.000000','ADMIN',NULL,NULL),(62,'JL MANGGA MUDA','2021-12-29 00:01:14.005000','ADMIN','tekmichan@gmail.com','TAKUMI KITAMURA','08123454367','2021-12-29 00:01:14.005000','ADMIN',NULL,NULL),(63,'TEST ADDRESSS','2021-12-29 00:08:47.453000','ADMIN','testuser@gmail.com','SUGAR HONEY','081456876987','2021-12-29 10:25:09.000000','ADMIN',NULL,NULL),(64,'JL BOULEVARD SUMMARECON GADING SERPONG','2021-12-29 09:21:48.584000','Admin','synystergroup@gmail.com','SYSNITER GATES','081234567876','2021-12-29 09:21:48.584000','Admin',NULL,NULL),(65,'Jl Sultan Jaya','2021-12-29 12:40:38.033000','Admin','paper','DOJA CAT','08123456789384','2021-12-29 12:40:38.033000','Admin',NULL,'2021-12-29 12:49:12.000000'),(67,'Jl Sultan Jaya','2021-12-29 12:46:35.985000','Admin','a','DOJA CATV','08123456789384','2021-12-29 12:46:35.985000','Admin',NULL,'2021-12-29 12:49:08.000000'),(69,'Jl Sultan Jaya','2021-12-29 12:48:14.792000','Admin','a','DOJA CATXV','08123456789384','2021-12-29 12:48:14.792000','Admin',NULL,'2021-12-29 12:49:06.000000'),(71,'JL KEBONAGUNG SLEMAN','2021-12-30 09:27:50.665000','Admin','a','LOLICA ','081234567890','2021-12-30 09:27:50.665000','Admin',NULL,NULL),(72,'JL MANGKUBUMI','2021-12-30 09:48:47.899000','Admin','cecebbibi@gmail.com','BUSINESS CENTRAL','081213456876','2021-12-30 09:49:26.000000','ADMIN',NULL,NULL),(73,'JL PARANGTRITIS','2021-12-30 09:52:31.966000','ADMIN','busybee@gmail.com','BUSY BEE FARM','081234567324','2021-12-30 09:52:31.966000','ADMIN',NULL,NULL),(74,'JL MONJALI','2021-12-30 09:57:37.587000','ADMIN','adot@gmail.com','ADITYA ADOT','08123456798','2021-12-30 09:57:37.587000','ADMIN',NULL,NULL),(75,'JL TEST ADDRESS USER','2021-12-30 10:10:17.455000','ADMIN','testuser@gmail.com','TEST USER','081234566789','2021-12-30 10:30:04.000000','ADMIN',NULL,NULL),(76,'TEST USER','2021-12-30 14:38:21.340000','ADMIN','aaurelia@gmail.com','IL SOGNO ','081234567789','2021-12-30 14:38:21.340000','ADMIN',NULL,NULL),(77,'DSDASDAD','2021-12-30 14:41:45.676000','ADMIN','aurelia@gmail.com','SDSAD','32372','2021-12-30 14:41:45.676000','ADMIN',NULL,NULL);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `principal`
--

DROP TABLE IF EXISTS `principal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `principal` (
  `id` int NOT NULL AUTO_INCREMENT,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `licensed_expiry_date` datetime(6) DEFAULT NULL,
  `principal_id` varchar(255) NOT NULL,
  `person_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_7fmkomwyptupyd244hu7fanxk` (`principal_id`),
  KEY `FKhsluqb3cv4o4ktcajodlq9cn1` (`person_id`),
  CONSTRAINT `FKhsluqb3cv4o4ktcajodlq9cn1` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `principal`
--

LOCK TABLES `principal` WRITE;
/*!40000 ALTER TABLE `principal` DISABLE KEYS */;
INSERT INTO `principal` VALUES (1,'Yogyakarta','Indonesia','(021)98767483','2025-12-02 00:00:00.000000','KONG',2),(2,'Yogyakarta','Indonesia','(021)857684','2023-12-21 00:00:00.000000','SOSRO',3),(3,'Yogyakarta','Indonesia','(021)5876345','2021-12-04 00:00:00.000000','AURE',7),(5,'Yogyakarta','Indonesia','(021)45643','2021-12-30 01:16:28.000000','IKHS',10),(14,'Surabaya','Indonesia','(021) 45673','2021-12-05 19:28:46.000000','MONA52',19),(15,'Yogyakarta','Indonesia','(021)76935','2021-03-03 00:00:00.000000','PAPE52',25),(16,'Yogyakarta','Indonesia','(021)76935','2021-03-02 00:00:00.000000','YACO68',36),(17,'BANTEN','INDONESIA','(012)43535','2021-03-02 00:00:00.000000','BELLALANG',37),(18,'BANTEN','INDONESIA','02134567','2021-12-31 20:18:14.000000','MADA62',38),(19,'YOGYAKARTA','INDONESIA','02134567','2021-12-31 21:44:19.998000','ANNI16',39),(20,'Yogyakarta','Indonesia','(021)76935','2021-12-31 20:18:14.000000','ASH29',40),(21,'AAAAA','AAAAA','9864234','2021-12-31 21:51:51.833000','AKAN24',41),(22,'BANTEN','INDONESIA','123456789','2021-12-30 00:00:00.000000','CHRI39',42),(23,'XXXXXXXXXXX','XXXXXXXXXX','123456','2021-12-27 00:00:00.000000','XXXX21',43),(24,'BANTEN','INDONESIA','(021)23456','2026-12-31 07:00:00.000000','JENN86',51),(25,'YOGYAKARTA','INDONESIA','(123)1234567','2027-12-29 00:00:00.000000','PRIN48',56),(26,'Yogyakarta','Indonesia','(021)76935','2032-12-28 00:00:00.000000','DOJA81',60),(27,'YOGYAKARTA','INDONESIA','021345678','2039-12-28 00:00:00.000000','NEZUKO',61),(28,'Yogyakarta','Indonesia','(021)76935','2021-03-03 07:00:00.000000','DOJA41',65),(29,'Yogyakarta','Indonesia','(021)76935','2021-03-03 07:00:00.000000','DOJA89',67),(30,'Yogyakarta','Indonesia','(021)76935','2021-03-03 07:00:00.000000','DOJA13',69),(31,'YOGYAKARTA','INDONESIA','021345678','2036-12-30 07:00:00.000000','BUSY82',73);
/*!40000 ALTER TABLE `principal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `disable_login` bit(1) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `registration_date` datetime(6) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `user_valid_thru` datetime(6) DEFAULT NULL,
  `distributor_id` int DEFAULT NULL,
  `person_id` int DEFAULT NULL,
  `principal_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_a3imlf41l37utmxiquukk8ajc` (`user_id`),
  KEY `FKetxjjm2lfl05ga2s0x39kdjy3` (`distributor_id`),
  KEY `FKir5g7yucydevmmc84i788jp79` (`person_id`),
  KEY `FKrx178o40me5juo8e8brxdq5pk` (`principal_id`),
  CONSTRAINT `FKetxjjm2lfl05ga2s0x39kdjy3` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`id`),
  CONSTRAINT `FKir5g7yucydevmmc84i788jp79` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FKrx178o40me5juo8e8brxdq5pk` FOREIGN KEY (`principal_id`) REFERENCES `principal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (4,_binary '\0','$2a$10$FL78UxsLuDlx3BL9Ct2znuX6RrchN2vaB6rVD49JIdCVQTC3Xp2aK','2021-12-06 11:15:16.056000','Regular','Sakuradesu','2021-12-23 07:00:00.000000',NULL,24,25),(7,_binary '\0','$2a$10$wWejiG9GSeToyN3QlwZ9..buG2EnJdC/EtanvB60bJ0LGkHxQYHve','2021-12-07 23:41:52.737000','Active','Horizon','2022-12-06 07:00:00.000000',NULL,30,1),(8,_binary '\0','$2a$10$OxKJsqB9/ZlDUhY1qhlnLu9OK/qNJp0c0Amj01GOcuT0LIeScbSh2','2021-12-07 23:49:05.713000','Regular','Horizan','2022-12-06 07:00:00.000000',NULL,32,1),(9,_binary '\0','$2a$10$0L4CRb8Lki4XUCUNQqCyw.bkUjPknUQS/9olEoCZIWX92rMXnJo3.','2021-12-07 23:51:38.674000','Active','Hormizan','2022-12-06 07:00:00.000000',NULL,34,1),(12,_binary '\0','$2a$10$3kLaiS1T3B3hyQ56xML0Pu99YmqIKS2gYtaoDOFDlFZVRLRA7umlW','2021-12-23 13:28:55.051000','1','DROWN','2021-12-23 07:00:00.000000',NULL,58,5),(13,_binary '\0','$2a$10$p5kwf3hASt0Om/RTeyCnOu3hl7WAPw4D9SY2N7BG4Jx.FqpKO/9TK','2021-12-29 00:01:14.169000','Regular','TEKMI','2022-03-26 07:00:00.000000',NULL,62,27),(14,_binary '\0','$2a$10$5fKkzw3JY0NaFKImBcbjQu6idxdQufuTAqDqZstY1BmjfJIAMMBri','2021-12-29 00:08:47.678000','Regular','TEST121','2021-12-31 07:00:00.000000',NULL,63,14),(15,_binary '\0','$2a$10$6AQ7nwpkxaV8mB1mCeDdtOZzkG0B51Mg1fmW81AmHM1Q.wJwWntH6','2021-12-30 09:57:37.840000','Regular','ADIT99','2021-12-31 07:00:00.000000',NULL,74,16),(16,_binary '','$2a$10$wQmf1CeH/c8VxIiCrI5/0eVFgRDpCGcQjFdfxtVpjJ4heQPrWa3F2','2021-12-30 10:10:17.677000','Regular','TEST30',NULL,11,75,27),(17,_binary '\0','$2a$10$gmcOL3euXai4XwbBsaYDvusznphtU3ue.1rbIgiBDvaaIbySNiIoe','2021-12-30 14:38:21.491000','Regular','ILSOG','2039-12-30 07:00:00.000000',NULL,76,16),(18,_binary '\0','$2a$10$EeI1ELk0EJ/1PAUtwZTNYeUhBAHuHGNow5JG93PoCPxJYlnJ3hAYi','2021-12-30 14:41:45.817000','Regular','AUREL',NULL,NULL,77,31);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-31 10:03:40
