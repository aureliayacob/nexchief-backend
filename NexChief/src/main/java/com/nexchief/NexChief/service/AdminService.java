package com.nexchief.NexChief.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.nexchief.NexChief.customfields.CustomAdmin;
import com.nexchief.NexChief.entity.Admin;
import com.nexchief.NexChief.repository.AdminRepository;
import com.nexchief.NexChief.response.Response;

@Service
public class AdminService {

	@Autowired
	AdminRepository repoAdmin;

	public ResponseEntity<Response> adminLogin(@RequestBody Admin payload){
		Optional<CustomAdmin.AdminLogin>  optAdmin = repoAdmin.findAdminByUsername(payload.getUsername());
		List<CustomAdmin.AdminLogin> emptyList =  new ArrayList<CustomAdmin.AdminLogin>();

		if(optAdmin.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Username does not exist" , emptyList)) ;

		} else {
			String inputtedPassword = payload.getPassword();
			Boolean passwordValid = BCrypt.checkpw(inputtedPassword, optAdmin.get().getPassword());

			if(!passwordValid) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),"Invalid Password", emptyList));

			}

			Optional<CustomAdmin.DataAdmin>dataToSend = repoAdmin.getDataAdminByUsername(payload.getUsername());

			return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "OK", dataToSend)) ;
			}

	}






}
