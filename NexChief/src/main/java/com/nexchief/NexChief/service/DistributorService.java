package com.nexchief.NexChief.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.nexchief.NexChief.dto.DistributorShowDto;
import com.nexchief.NexChief.customfields.CustomPrincipal;
import com.nexchief.NexChief.customfields.CustomDistributor.GetFullDataDistributor;
import com.nexchief.NexChief.dto.DistributorCreateUpdateDto;
import com.nexchief.NexChief.entity.Distributor;
import com.nexchief.NexChief.entity.Principal;
import com.nexchief.NexChief.repository.DistributorRepository;
import com.nexchief.NexChief.repository.PersonRepository;
import com.nexchief.NexChief.repository.PrincipalRepository;
import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.utils.Generator;


@Service
public class DistributorService {
	@Autowired
	DistributorRepository repoDistributor;

	@Autowired
	PrincipalRepository repoPrincipal;

	@Autowired
	PersonRepository repoPerson;

	String emailRegex = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	String idRegex = "^([A-Z]{5,12}|[A-Z0-9]{5,12})$";

	public ResponseEntity<Response> getDistributorPage(int size, int page){
		Pageable pageable = PageRequest.of(page, size, Sort.by("pr.name"));
		List<GetFullDataDistributor> allDistributor = repoDistributor.getAllDistributorsPage(pageable);
		if(allDistributor.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(HttpStatus.NOT_FOUND.value(), "Data Not Found")) ;
		}

		List<DistributorShowDto> distributorDto = new ArrayList<DistributorShowDto>();

		for(GetFullDataDistributor d : allDistributor) {
			Optional<CustomPrincipal.GetFullDataPrincipal> principal = repoPrincipal.getPrincipalById(d.getPrincipalId());
			DistributorShowDto newDto = new DistributorShowDto(d, principal.get());
			distributorDto.add(newDto);
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Found", distributorDto));
	}

	public ResponseEntity<Response> getAllDistributor(){
		List<GetFullDataDistributor> allDistributor = repoDistributor.getAllDistributors();
		if(allDistributor.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(HttpStatus.NOT_FOUND.value(), "Data Not Found")) ;
		}
		List<DistributorShowDto> distributorDto = new ArrayList<DistributorShowDto>();
		for(GetFullDataDistributor d : allDistributor) {
			Optional<CustomPrincipal.GetFullDataPrincipal> principal = repoPrincipal.getPrincipalById(d.getPrincipalId());
			DistributorShowDto newDto = new DistributorShowDto(d, principal.get());
			distributorDto.add(newDto);
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Found", distributorDto));
	}

	public ResponseEntity<Response> getAllDistributorPerPrincipal(int principalId){
		List<GetFullDataDistributor> allDistributor = repoDistributor.getAllDistributorsPerPrincipal(principalId);
		if(allDistributor.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(HttpStatus.NOT_FOUND.value(), "Data Not Found")) ;
		}
		List<DistributorShowDto> distributorDto = new ArrayList<DistributorShowDto>();
		for(GetFullDataDistributor d : allDistributor) {
			Optional<CustomPrincipal.GetFullDataPrincipal> principal = repoPrincipal.getPrincipalById(d.getPrincipalId());
			DistributorShowDto newDto = new DistributorShowDto(d, principal.get());
			distributorDto.add(newDto);
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Found", distributorDto));
	}

	public ResponseEntity<Response> getDistributorByDistributorId(@RequestParam("id") String distributorId){
		Optional<GetFullDataDistributor> distributor = repoDistributor.getDistributorByDistributorId(distributorId);
		if(distributor.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(HttpStatus.NOT_FOUND.value(), "Data Not Found")) ;
		}



		Optional<CustomPrincipal.GetFullDataPrincipal> principal = repoPrincipal.getPrincipalById(distributor.get().getPrincipalId());
		DistributorShowDto newDto = new DistributorShowDto(distributor.get(), principal.get());


		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Found", newDto));
	}

	public ResponseEntity<Response> addDistributor(@RequestBody DistributorCreateUpdateDto payload){
		
		//check if required fields are filled
		if(payload.getName() == "" || payload.getAddress() == "" || payload.getCity() == "" 
				|| payload.getOwnerFirstName() == "" || payload.getOwnerLastName() == "" || payload.getPhone() == "") {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Field cannot be empty")) ;
		}

		//check if email is empty, if its not empty then check the format
		if(payload.getEmail()!="") {
			boolean emailIsValid = Pattern.compile(emailRegex).matcher(payload.getEmail()).matches();
			if(!emailIsValid) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Email format is invalid"));
			}
		}
		
		//get selected principal
		Optional<Principal> principal = repoPrincipal.getPrincipalObjById(payload.getPrincipalId());

		if(!principal.isPresent()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Principal doesn't exist"));
		}

		//add person(identity) to repository
		int personId = 0;
		int saveDistributor = 0;
		int savePerson = repoPerson.addPerson(payload.getAddress(), payload.getCreatedBy(), payload.getEmail(),payload.getName(), payload.getPhone(), payload.getUpdatedBy());
		if(savePerson > 0) {
			personId = repoPerson.getLastestPersonId();
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Add Distributor Failed")) ;
		}
		
		//		Person person = new Person(payload.getName().toUpperCase(), payload.getAddress(), payload.getPhone(), payload.getEmail(), "Admin", "Admin");
		//		Distributor distributor = new Distributor();

		//check if principalId fields is empty, it will generates ID when principalId fields empty
		if(payload.getDistributorId() == "") {
			String distributorId = Generator.generateDistributorId(payload.getName(), repoDistributor);
			//			distributor = new Distributor(distributorId, payload.getCity(), payload.getOwnerFirstName(),
			//					payload.getOwnerLastName(), payload.getFax(), payload.getCountry(), payload.getWebsite(), person, principal.get());
			saveDistributor = repoDistributor.insertDistributor(payload.getCity(), payload.getCountry(), distributorId, payload.getFax(), 
					payload.getOwnerFirstName(), payload.getOwnerLastName(), payload.getWebsite(), personId, principal.get().getId());

		} else {
			Optional<GetFullDataDistributor> checkDistributorId =  repoDistributor.getDistributorByDistributorId(payload.getDistributorId());

			//check if filled principalId already exist
			if(!checkDistributorId.isEmpty()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "ID Already Exist")) ;
			}else if(!Pattern.compile(idRegex).matcher(payload.getDistributorId()).matches()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "ID Format Invalid, ID must contain 5 to 12 letters or number")) ;
			}

			//			distributor = new Distributor(payload.getDistributorId().toLowerCase(), payload.getCity(), payload.getOwnerFirstName(),
			//					payload.getOwnerLastName(), payload.getFax(), payload.getCountry(), payload.getWebsite(), person, principal.get());
			saveDistributor = repoDistributor.insertDistributor(payload.getCity(), payload.getCountry(), payload.getDistributorId(), payload.getFax(), 
					payload.getOwnerFirstName(), payload.getOwnerLastName(), payload.getWebsite(), personId, principal.get().getId());
		}

		//		Person savePerson = repoPerson.save(person);
		//		Distributor saveDistributor = repoDistributor.save(distributor);


		if(saveDistributor < 1 ) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Add Distributor Failed")) ;
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "New Distributor Added")) ;
		}
	}

	public ResponseEntity<Response> deleteDistributor(@PathVariable(value="id") int id){
		int deletedDistributor = repoDistributor.deleteDistributor(id);
		if(deletedDistributor < 1) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Delete Principal Failed")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Distributor Deleted")) ;
	}

	public ResponseEntity<Response> searchDistributor(@RequestParam("name") String name){

		List<GetFullDataDistributor> getDistributor =  repoDistributor.searchByName(name);
		if(getDistributor.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(HttpStatus.NOT_FOUND.value(), "Data Not Found")) ;
		}

		List<DistributorShowDto> distributorDto = new ArrayList<DistributorShowDto>();

		for(GetFullDataDistributor d : getDistributor) {
			Optional<CustomPrincipal.GetFullDataPrincipal> principal = repoPrincipal.getPrincipalById(d.getPrincipalId());
			DistributorShowDto newDto = new DistributorShowDto(d, principal.get());
			distributorDto.add(newDto);
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Found", distributorDto));
	}

	public ResponseEntity<Response> editDistributor(@PathVariable(value="id") int id, @RequestBody DistributorCreateUpdateDto payload){
		int principalId;

		if(payload.getName() == "" || payload.getAddress() == "" || payload.getCity() == "" 
				|| payload.getOwnerFirstName() == "" || payload.getOwnerLastName() == "" || payload.getPhone() == "" || 
				payload.getPrincipalId() == null ) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Field cannot be empty")) ;
		}

		if(payload.getEmail()!="") {
			Boolean emailIsValid = Pattern.compile(emailRegex).matcher(payload.getEmail()).matches();
			if(!emailIsValid) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Email format is invalid"));
			}
		}


		Optional<Distributor> oldDistributor = repoDistributor.getDistributorById(id);

		if(oldDistributor.get().getPrincipal().getId() == payload.getPrincipalId()) {
			principalId = oldDistributor.get().getPrincipal().getId();
		}else {

			Optional<Principal> checkPrincipal = repoPrincipal.getPrincipalObjById(payload.getPrincipalId());

			if (!checkPrincipal.isEmpty()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Principal doesnt exist")) ;
			} else {
				principalId = checkPrincipal.get().getId( );
			}}

		int updatedPerson = repoPerson.updateDistributor(id, payload.getName().toUpperCase(), payload.getAddress(), payload.getEmail(), payload.getPhone(), payload.getUpdatedBy());
		int updatedDistributor = repoDistributor.updateDistributor(payload.getCity(), payload.getCountry(), payload.getFax(), 
				payload.getOwnerFirstName(), payload.getOwnerLastName(), payload.getWebsite(), 
				principalId, id);
		if(updatedDistributor < 1 || updatedPerson < 1) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Edit Distributor Failed")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Distributor Data Updated")) ;
	}

	public ResponseEntity<Response> countDistributor(){
		int countedDistributor = repoDistributor.countDistributor();
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Distributor Counted", countedDistributor)) ;

	}

}
