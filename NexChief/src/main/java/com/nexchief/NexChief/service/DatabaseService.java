package com.nexchief.NexChief.service;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.nexchief.NexChief.entity.DbRecord;
import com.nexchief.NexChief.repository.DbRecordRespository;
import com.nexchief.NexChief.response.Response;

@Service
public class DatabaseService {

	@Autowired
	DbRecordRespository repoDbRecord; 

	/*Backup Db to .sql, Store it to Project's Jar Directory*/
	public ResponseEntity<Response> backupDb() throws IOException, URISyntaxException, InterruptedException {

		/*NOTE: Getting path to the Jar file being executed*/
		/*NOTE: YourImplementingClass-> replace with the class executing the code*/


		CodeSource codeSource = DatabaseService.class.getProtectionDomain().getCodeSource();
		File jarFile = new File(codeSource.getLocation().toURI().getPath());
		String jarDir = jarFile.getParentFile().getParentFile().getPath();
		System.out.println(jarDir);

		/*NOTE: Creating Database Backup Name*/
		LocalDateTime endOfDay = LocalDate.now().atTime(LocalTime.MAX);
		LocalDateTime startOfDay = LocalDate.now().atTime(LocalTime.MIN);
		LocalDateTime backupTime = LocalDateTime.now();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH.mm.ss");
		DateTimeFormatter formatterToDate = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String dateStamp = backupTime.format(formatter);

		DbRecord dbRecord = repoDbRecord.getLastestRecord();
		String fileName;

		//default backup version is v1
		fileName = dateStamp+"_nexchiefdb_backup_v1.sql";

		//versioning backup
		if(dbRecord!=null) {
			String[] getName = dbRecord.getFileName().split("_");
			String getDateString = getName[0] +" "+getName[1].replaceAll("\\.", ":");
			LocalDateTime prevBackupDate = LocalDateTime.parse(getDateString, formatterToDate);

			/*cek jika hari ini sudah pernah melakukan backup*/
			if(prevBackupDate.isBefore(endOfDay) && prevBackupDate.isAfter(startOfDay)) {
				System.out.println("MASUk");
				//version nambah
				int getVersion = (Integer.parseInt(getName[getName.length-1].substring(1, 2)))+1;
				fileName = dateStamp+"_nexchiefdb_backup_v"+String.valueOf(getVersion)+".sql";
			}
		}

		/*NOTE: Creating Database Constraints*/
		String dbName = "nexchief";
		String dbUser = "root";
		String dbPass = "Manggis999";

		final String pathUpload = "src\\main\\resources\\static\\";
		String savePath = "\"" + pathUpload + "backupdb\\"+fileName+"\"";	

		/*NOTE: Used to create a cmd command*/
		String executeCmd = "mysqldump -u" + dbUser + " -p" + dbPass + " --databases " + dbName +" -r " + savePath;

		/*NOTE: Executing the command here*/
		Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);

		int processComplete = runtimeProcess.waitFor();
		System.out.println("Process Complete "+processComplete);
		/*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
		if (processComplete == 0) {
			DbRecord newRecord = new DbRecord(fileName, savePath);
			repoDbRecord.recordBackup(newRecord.getFileName(), newRecord.getFilePath(), newRecord.getLastUpdate());
		
			return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Backup Success", fileName));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response(HttpStatus.INTERNAL_SERVER_ERROR
					.value(), "Backup Failed"));
		}

	}

	public ResponseEntity<Response> getLastestRecord(){
		DbRecord getRecord = repoDbRecord.getLastestRecord();
		
		if(getRecord==null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), 
					"Record Not Found"));
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Get Record Success", getRecord));

		}

	}

	public InputStreamResource downloadFile (HttpServletResponse response, @PathVariable("file")String filename) throws IOException, URISyntaxException {
		ClassPathResource  backupFileDir;
		InputStreamResource resource = null;
		try {
			backupFileDir= new ClassPathResource("static/backupdb/"+filename);
			System.out.println(backupFileDir.getPath());
			resource = new InputStreamResource(backupFileDir.getInputStream());

		}catch (Exception e) {
			e.printStackTrace();
		}
		response.setContentType("application/sql");
		response.setHeader("Content-Disposition", "attachment; filename="+filename);
		return resource;
	}

	public ResponseEntity<Response> checkFileExistence(String filename) throws IOException, URISyntaxException {
		ClassPathResource  backupFileDir;

		try {
			
			backupFileDir = new ClassPathResource("static/backupdb/"+filename);
			if(backupFileDir.exists()) {
				return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK
						.value(), "File Exist"));
			} 
			
		}catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			System.out.println("Exception");
	
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(HttpStatus.NOT_FOUND
				.value(), "File Doesn't Exist"));

	}

}






