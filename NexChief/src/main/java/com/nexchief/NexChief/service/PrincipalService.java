package com.nexchief.NexChief.service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.nexchief.NexChief.customfields.CustomPrincipal.GetFullDataPrincipal;
import com.nexchief.NexChief.dto.PrincipalDto;
import com.nexchief.NexChief.repository.PersonRepository;
import com.nexchief.NexChief.repository.PrincipalRepository;
import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.utils.Generator;

@Service
public class PrincipalService {
	@Autowired
	PrincipalRepository repoPrincipal;

	@Autowired
	PersonRepository repoPerson;

	String emailRegex = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	String idRegex = "^([A-Z]{5,12}|[A-Z0-9]{5,12})$";

	public ResponseEntity<Response> getAllPrincipalPage( int size, int page){
		Pageable pageable = PageRequest.of(page, size, Sort.by("pr.name"));
		List<GetFullDataPrincipal> principalPage =  repoPrincipal.getAllPrincipalsPage(pageable);
		if(principalPage.size()==0) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Data Not Found")) ;

		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "OK", principalPage)) ;
	}

	public ResponseEntity<Response> getAllPrincipal(){
		List<GetFullDataPrincipal> allPrincipal =  repoPrincipal.getAllPrincipals();
		if(allPrincipal.size()==0) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Data Not Found")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "OK", allPrincipal)) ;
	}

	public ResponseEntity<Response> getPrincipalByPrincipalId(@RequestParam("id") String principalId){
		System.out.println("-->"+principalId);

		Optional<GetFullDataPrincipal> getPrincipal =  repoPrincipal.getPrincipalByPrincipalId(principalId);
		if(getPrincipal.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(HttpStatus.NOT_FOUND.value(), "Data Not Found")) ;

		}

		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "OK", getPrincipal.get())) ;
	}

	public ResponseEntity<Response> searchPrincipalByName(@RequestParam("name") String name){

		List<GetFullDataPrincipal> getPrincipal =  repoPrincipal.searchPrincipalByName(name);

		if(getPrincipal.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Response(HttpStatus.NOT_FOUND.value(), "Data Not Found")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "OK", getPrincipal)) ;
	}

	public ResponseEntity<Response> deletePrincipal(@PathVariable(value="id") int id){
		int deletedPrincipal = repoPrincipal.deletePrincipal(id);

		if(deletedPrincipal < 1) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Delete Principal Failed")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Principal Deleted")) ;
	}

	public ResponseEntity<Response> editPrincipal(@PathVariable(value="id") int id, @RequestBody PrincipalDto payload){
		if(payload.getName() == "" || payload.getAddress() == "" || payload.getCity() == "" ||payload.getEmail() == "" ||payload.getPhone() == "" || payload.getLicensedExpiryDate() == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Fields cannot be empty")) ;
		}

		Boolean emailIsValid = Pattern.compile(emailRegex).matcher(payload.getEmail()).matches();
		if(!emailIsValid) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Email format is invalid"));
		}

		int updatedPerson = repoPerson.updatePrincipal(id, payload.getName().toUpperCase(), payload.getAddress(), payload.getEmail(), payload.getPhone(), payload.getUpdatedBy());
		int updatedPrincipal = repoPrincipal.updatePrincipal(payload.getCity(), payload.getCountry(), payload.getFax(), payload.getLicensedExpiryDate(), id);
		if( updatedPrincipal < 1||updatedPerson < 1 ) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Edit Failed")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Updated")) ;
	}

	public ResponseEntity<Response> addPrincipal(@RequestBody PrincipalDto payload){
		String principalId;

		//check if required fields are filled
		if(payload.getName() == "" || payload.getAddress() == "" || payload.getCity() == "" ||payload.getEmail() == "" ||payload.getPhone() == "") {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Name, Address, City, Email, Phone fields are required")) ;
		}

		//check if email is valid
		boolean emailIsValid = Pattern.compile(emailRegex).matcher(payload.getEmail()).matches();
		if(!emailIsValid) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Email format is invalid"));
		}
		
		//add person(identity) to repository
		int savePerson = repoPerson.addPerson(payload.getAddress(), payload.getCreatedBy(), payload.getEmail(),payload.getName(), payload.getPhone(),payload.getUpdatedBy());
		int savePrincipal = 0;
		
		if(savePerson > 0) {
			int idPerson = repoPerson.getLastestPersonId();

			//check if principalId fields is empty, it will generates ID when principalId fields empty
			if(payload.getPrincipalId() == "") {
				principalId = Generator.generatePrincipalId(payload.getName(), repoPrincipal);
				savePrincipal = repoPrincipal.addPrincipal(payload.getCity(), payload.getCountry(), payload.getFax(), payload.getLicensedExpiryDate(), principalId, idPerson);
				
			} else {

				Optional<GetFullDataPrincipal> checkPrincipalId =  repoPrincipal.getPrincipalByPrincipalId(payload.getPrincipalId());
				
				//check if filled principalId already exist
				if(!checkPrincipalId.isEmpty()) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "ID Already Exist")) ;
				}else if(!Pattern.compile(idRegex).matcher(payload.getPrincipalId()).matches()) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "ID Format Invalid, ID must contain ID must contain 5 to 12 letters or number")) ;
				}

				savePrincipal = repoPrincipal.addPrincipal(payload.getCity(), payload.getCountry(), payload.getFax(), payload.getLicensedExpiryDate(), payload.getPrincipalId(), idPerson);

			}
		}
		if(savePrincipal < 1 ) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Add Distributor Failed")) ;
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "New Distributor Added")) ;
		}	}


	public ResponseEntity<Response> countActivePrincipal(){
		int countPrincipal = repoPrincipal.countPrincipals();
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(),  "Counted", countPrincipal)) ;
	}

	public ResponseEntity<Response> isHaveDistributor(int id){
		int countDistributor = repoPrincipal.countDistributor(id);
		if(countDistributor==0) {
			return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(),  "Principal doesn't have any Distributor", countDistributor)) ;
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(),  "Principal have Distributor", countDistributor)) ;
		}

	}


}