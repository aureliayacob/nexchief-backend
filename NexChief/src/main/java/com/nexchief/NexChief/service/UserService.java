package com.nexchief.NexChief.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.nexchief.NexChief.customfields.CustomPrincipal;
import com.nexchief.NexChief.customfields.CustomDistributor.GetFullDataDistributor;
import com.nexchief.NexChief.customfields.CustomUser.GetFullDataUser;
import com.nexchief.NexChief.dto.UserCreateUpdateDto;
import com.nexchief.NexChief.dto.UserShowDto;
import com.nexchief.NexChief.entity.Distributor;
import com.nexchief.NexChief.entity.Principal;
import com.nexchief.NexChief.repository.DistributorRepository;
import com.nexchief.NexChief.repository.PersonRepository;
import com.nexchief.NexChief.repository.PrincipalRepository;
import com.nexchief.NexChief.repository.UserRepository;
import com.nexchief.NexChief.response.Response;

@Service
public class UserService {
	@Autowired
	UserRepository repoUser;

	@Autowired
	PrincipalRepository repoPrincipal;

	@Autowired
	DistributorRepository repoDistributor;

	@Autowired
	PersonRepository repoPerson;

	String emailRegex = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	String idRegex = "^([A-Z]{5,12}|[A-Z0-9]{5,12})$";


	public ResponseEntity<Response> getAllUserPage(int page, int size){
		Pageable pageable = PageRequest.of(page, size, Sort.by("pr.name"));
		List<GetFullDataUser> allUser = repoUser.getAllUserPage(pageable);

		List<UserShowDto> allUserDto = new ArrayList<>();
		for(GetFullDataUser u : allUser) {
			UserShowDto userDto ;
			Optional<CustomPrincipal.GetFullDataPrincipal> principal = repoPrincipal.getPrincipalById(u.getPrincipalId());
			if(u.getDistributorId() != null) {
				Optional<GetFullDataDistributor> distributor = repoDistributor.getCustomDistributorById(u.getDistributorId());
				userDto = new UserShowDto(u, principal.get(), distributor.get());
			}else {
				userDto = new UserShowDto(u, principal.get(), null);
			}

			allUserDto.add(userDto);
		}

		if(allUser.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Data Not Found")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Found", allUserDto));
	}

	public ResponseEntity<Response> getUserById(int id){
		Optional<GetFullDataUser> allUser = repoUser.getUserById(id);
		UserShowDto userDto ;


		if(allUser.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Data Not Found")) ;
		}

		Optional<CustomPrincipal.GetFullDataPrincipal> principal = repoPrincipal.getPrincipalById(allUser.get().getPrincipalId());

		if(allUser.get().getDistributorId() != null) {
			Optional<GetFullDataDistributor> distributor = repoDistributor.getCustomDistributorById(allUser.get().getDistributorId());
			userDto = new UserShowDto(allUser.get(), principal.get(), distributor.get());
		}else {
			userDto = new UserShowDto(allUser.get(), principal.get(), null);
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Found", userDto));
	}

	public ResponseEntity<Response> getAllUser(){
		List<GetFullDataUser> allUser = repoUser.getAllUser();

		List<UserShowDto> allUserDto = new ArrayList<>();
		for(GetFullDataUser u : allUser) {
			UserShowDto userDto ;
			Optional<CustomPrincipal.GetFullDataPrincipal> principal = repoPrincipal.getPrincipalById(u.getPrincipalId());
			if(u.getDistributorId() != null) {
				Optional<GetFullDataDistributor> distributor = repoDistributor.getCustomDistributorById(u.getDistributorId());
				userDto = new UserShowDto(u, principal.get(), distributor.get());
			}else {
				userDto = new UserShowDto(u, principal.get(), null);
			}

			allUserDto.add(userDto);
		}


		if(allUser.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Data Not Found")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Found", allUserDto));
	}

	public ResponseEntity<Response> addUser(UserCreateUpdateDto payload){
//		User user;
		Distributor distributor;

		//check if required fields are filled
		if(payload.getUserId() == "" || payload.getName() == "" || payload.getPassword() == "" || payload.getAddress() == "" || payload.getPhone() == "" ||
				payload.getPrincipalId() == null){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Field cannot be empty")) ;
		}

		//check if email is empty, if its not empty then check the format
		if(payload.getEmail()!="") {
			Boolean emailIsValid = Pattern.compile(emailRegex).matcher(payload.getEmail()).matches();
			if(!emailIsValid) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Email format is invalid"));
			}

		}

		//get selected principal
		Optional<Principal> principal = repoPrincipal.getPrincipalObjById(payload.getPrincipalId());

		if(!principal.isPresent()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Principal doesn't exist"));
		}

		//check if password format is valid, and then hash password before store it to db
		String hashPassword;
		if(passwordValid(payload.getPassword())){
			hashPassword = BCrypt.hashpw(payload.getPassword(), BCrypt.gensalt());
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Invalid Password, Password must be minimum 8 characters, at least 1  uppercase letter and 1 number")) ;

		}

		//check if user id format is valid and unique
		if(!userIdFormat(payload.getUserId())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Username format invalid : Username consists of minimum 5 to 12 alphanumeric character")) ;
		}else if(!userIdValid(payload.getUserId())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "ID Already Exist")) ;
		}


		//check if user chose the distributor
		Integer distributorId = null;
		if(payload.getDistributorId() != null) {
			distributor = repoDistributor.getDistributorById(payload.getDistributorId()).get();
			distributorId = distributor.getId();
		}


		//add person(identity) to repository
		int personId = 0;
		int saveUser = 0;
		int savePerson = repoPerson.addPerson(payload.getAddress(), payload.getCreatedBy(), payload.getEmail(),payload.getName(), payload.getPhone(), payload.getUpdatedBy());
		if(savePerson > 0) {
			personId = repoPerson.getLastestPersonId();
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Add Distributor Failed")) ;
		}

		//		user = new User(payload.getUserId().toLowerCase(), hashPassword, payload.getStatus(), payload.isDisableLogin(), 
		//				payload.getUserValidThru(), person, distributor, principal);

		//		Person savePerson = repoPerson.save(person);
		//		User saveUser = repoUser.save(user);
		saveUser = repoUser.insertUser(payload.isDisableLogin(), hashPassword, payload.getStatus(), payload.getUserId(), payload.getUserValidThru(), distributorId, personId, principal.get().getId());

		if(saveUser < 1 ) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Data Not Found")) ;

		}else {
			return ResponseEntity.status(HttpStatus.OK).body(new Response(200, "New Data Added")) ;
		}
	}

	public ResponseEntity<Response> hardDeleteUser(int id){
		Optional<GetFullDataUser> getUser = repoUser.getUserById(id);
		if(getUser.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "User Not Found")) ;
		}
		int getPersonId = getUser.get().getPersonId();
		int deletedUser = repoUser.hardDeleteUser(id);
		int deletePerson = repoPerson.hardDeleteUserPerson(getPersonId);

		if(deletedUser < 1 || deletePerson <1) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Delete Failed")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Delete Success")) ;
	}

	public ResponseEntity<Response> softDeleteUser(int id){

		int deletedUser = repoUser.deleteUser(id);
		if(deletedUser < 1) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Delete Failed, User Not Found")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Delete Success")) ;
	}

	public ResponseEntity<Response> searchUser(String keyword){
		List<GetFullDataUser> getUser = repoUser.searchUser(keyword);
		List<UserShowDto> allUserDto = new ArrayList<>();
		for(GetFullDataUser u : getUser) {
			UserShowDto userDto ;
			Optional<CustomPrincipal.GetFullDataPrincipal> principal = repoPrincipal.getPrincipalById(u.getPrincipalId());
			if(u.getDistributorId() != null) {
				Optional<GetFullDataDistributor> distributor = repoDistributor.getCustomDistributorById(u.getDistributorId());
				userDto = new UserShowDto(u, principal.get(), distributor.get());
			}else {
				userDto = new UserShowDto(u, principal.get(), null);
			}

			allUserDto.add(userDto);
		}

		if(getUser.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Data Not Found")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "Data Found", allUserDto));
	}

	public ResponseEntity<Response> editUser(int id, UserCreateUpdateDto payload){
		int updatedPerson;
		int updatedUser;
		Integer distributorId;
		if(payload.getName() == "" || payload.getPassword() == "" || payload.getAddress() == "" || payload.getPhone() == "" ||
				payload.getPrincipalId() == null){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(), "Field cannot be empty")) ;
		}

		if(payload.getEmail()!="") {
			boolean emailIsValid = Pattern.compile(emailRegex).matcher(payload.getEmail()).matches();
			if(!emailIsValid) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(HttpStatus.BAD_REQUEST.value(),  "Email format is invalid"));
			}
		}

		if( payload.getDistributorId() ==null) {
			distributorId = null;
		}else {
			distributorId= payload.getDistributorId();
		}


		updatedPerson = repoPerson.updateUser(id, payload.getName().toUpperCase(), payload.getAddress(), payload.getEmail(), payload.getPhone(), payload.getUpdatedBy());

		System.out.println("IS DISABLE LOGIN----->"+payload.isDisableLogin());
		updatedUser = repoUser.updateUser(payload.isDisableLogin(),payload.getStatus(), payload.getUserValidThru(), distributorId, payload.getPrincipalId(), id);

		if(updatedUser < 1 || updatedPerson < 1) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Update User Failed")) ;
		}
		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "User Data Updated")) ;
	}

	public ResponseEntity<Response> countUser(){
		int countedUser=repoUser.countUser();

		return ResponseEntity.status(HttpStatus.OK).body(new Response(HttpStatus.OK.value(), "User Counted", countedUser)) ;
	}

	public boolean userIdValid(String userId) {
		List<GetFullDataUser> checkUserId =  repoUser.getUserByUserId(userId);
		if(checkUserId.isEmpty()) {
			return true; 
		}
		return false;
	}

	public boolean userIdFormat(String id) {
		boolean check = Pattern.matches("^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$", id);
		return check;

	}

	public boolean passwordValid(String password) {
		boolean checkPassword = Pattern.matches("^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)[A-Za-z\\d]{8,}$", password);
		return checkPassword;

	}


}
