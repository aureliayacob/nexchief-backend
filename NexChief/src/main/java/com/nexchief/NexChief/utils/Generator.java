package com.nexchief.NexChief.utils;

import java.util.Random;

import com.nexchief.NexChief.repository.DistributorRepository;
import com.nexchief.NexChief.repository.PrincipalRepository;

public class Generator {

	public static String generatePrincipalId(String usn, PrincipalRepository repoPrincipal) {
		String[] splittedUserName = usn.split(" ");
		String firstWord;
		if(splittedUserName[0].length()>4) {
			firstWord = splittedUserName[0].substring(0, 4);
		}else {
			firstWord =  splittedUserName[0];	
		}

		Random rand = new Random();
		String newId = firstWord;

		do { 
			int randomInt = rand.nextInt(100);
			newId = firstWord.toUpperCase() + Integer.toString(randomInt);
		} while (!repoPrincipal.getPrincipalByPrincipalId(newId).isEmpty());

		return newId.toUpperCase();
	}

	public static String generateDistributorId(String usn, DistributorRepository repoDistributor) {
		String[] splittedUserName = usn.split(" ");
		String firstWord;
		if(splittedUserName[0].length()>4) {
			firstWord = splittedUserName[0].substring(0, 4);
		}else {
			firstWord =  splittedUserName[0];	
		}

		Random rand = new Random();
		String newId = firstWord;

		do {
			int randomInt = rand.nextInt(100);
			newId = firstWord.toUpperCase() + Integer.toString(randomInt);
		} while (!repoDistributor.getDistributorByDistributorId(newId).isEmpty());

		return newId.toUpperCase();
	}

	
}
