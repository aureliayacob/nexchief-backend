 package com.nexchief.NexChief.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.service.DistributorService;
import com.nexchief.NexChief.dto.DistributorCreateUpdateDto;
import com.nexchief.NexChief.repository.DistributorRepository;
import com.nexchief.NexChief.repository.PersonRepository;
import com.nexchief.NexChief.repository.PrincipalRepository;

@RestController
public class DistributorController {

	@Autowired
	DistributorRepository repoDistributor;

	@Autowired
	PrincipalRepository repoPrincipal;

	@Autowired
	PersonRepository repoPerson;

	@Autowired
	DistributorService serviceDistributor;

	
	@GetMapping(value="/api/distributor/{size}/{page}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getAllDistributorPage(@PathVariable("size") int size, @PathVariable("page") int page){
		return serviceDistributor.getDistributorPage(size, page);
	}
	
	@GetMapping(value="/api/distributor/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getAllDistributor(){
		return serviceDistributor.getAllDistributor();
	}
	
	@GetMapping(value="/api/distributors/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getAllDistributorPerPrincipal(@PathVariable("id") int id){
		return serviceDistributor.getAllDistributorPerPrincipal(id);
	}

	@GetMapping(value="/api/distributor", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getDistributorByDistributorId(@RequestParam("id") String distributorId){
		return serviceDistributor.getDistributorByDistributorId(distributorId);
	}

	@PostMapping(value="/api/distributor/new", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> addDistributor(@RequestBody DistributorCreateUpdateDto payload){
		return serviceDistributor.addDistributor(payload);
	}


	@DeleteMapping (value="/api/distributor/delete/{id}")
	public ResponseEntity<Response> deleteDistributor(@PathVariable(value="id") int id){
		return serviceDistributor.deleteDistributor(id);
	}

	@GetMapping (value="/api/distributor/search",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> searchDistributor(@RequestParam("name") String name){
		return serviceDistributor.searchDistributor(name);
	}


	@PutMapping (value="/api/distributor/edit/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> editDistributor(@PathVariable(value="id") int id, @RequestBody DistributorCreateUpdateDto payload){
		return serviceDistributor.editDistributor(id, payload);
	}

	@GetMapping (value="/api/distributors/count")
	public ResponseEntity<Response> countDistributor(){
		return serviceDistributor.countDistributor();
	}


}
