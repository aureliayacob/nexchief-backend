package com.nexchief.NexChief.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.service.DatabaseService;

@RestController
public class DatabaseController {
	
	@Autowired
	DatabaseService serviceDatabase;

	@GetMapping(value = "api/download/{file}")
	public InputStreamResource downloadFile (HttpServletResponse response, @PathVariable("file")String file) throws IOException, URISyntaxException {
		return serviceDatabase.downloadFile(response, file);
	}
	
	@GetMapping(value="/api/database/backup")
	public ResponseEntity<Response> backupDatabase() throws IOException, URISyntaxException, InterruptedException{
		return serviceDatabase.backupDb();
	}
	
	@GetMapping(value="/api/database/getrecord",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getBackupRecord(){
		return serviceDatabase.getLastestRecord();
	}
	
	@GetMapping(value="/api/database/checkfile/{filename}",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> chekFileExistence(@PathVariable("filename")String filename) throws IOException, URISyntaxException{
		return serviceDatabase.checkFileExistence(filename);
	}



}
