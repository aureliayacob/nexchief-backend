package com.nexchief.NexChief.controller;

import com.nexchief.NexChief.entity.Admin;
import com.nexchief.NexChief.response.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nexchief.NexChief.service.AdminService;

@RestController
public class AdminController {
	
	@Autowired
	AdminService serviceAdmin;
	
	@PostMapping (value="/api/auth/login",  consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> adminLogin(@RequestBody Admin payload){
		return serviceAdmin.adminLogin(payload);
		
	}
	


}
