package com.nexchief.NexChief.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nexchief.NexChief.dto.UserCreateUpdateDto;
import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService serviceUser;

	
	@GetMapping(value="/api/user/{size}/{page}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getAllUserPage(@PathVariable("page") int page, @PathVariable("size") int size){
		return serviceUser.getAllUserPage(page, size);
	}
	
	@GetMapping(value="/api/user/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getAllUser(){
		return serviceUser.getAllUser();
	}

	@GetMapping(value="/api/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getUserById(@PathVariable("id") int id){
		return serviceUser.getUserById(id);
	}
	
	@PostMapping(value="/api/user/new", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> addUser(@RequestBody UserCreateUpdateDto payload){
		return serviceUser.addUser(payload);
	}

	@DeleteMapping (value="/api/user/delete/{id}")
	public ResponseEntity<Response> deleteUser(@PathVariable(value="id") int id){
		return serviceUser.hardDeleteUser(id);
	}

	@GetMapping (value="/api/user/search",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> searchUser(@RequestParam("keyword") String keyword){
		return serviceUser.searchUser(keyword);
	}

	@PutMapping (value="/api/user/edit/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> editUser(@PathVariable(value="id") int id, @RequestBody UserCreateUpdateDto payload){
		return serviceUser.editUser(id, payload);
	}
	
	@GetMapping(value="/api/users/count")
	public ResponseEntity<Response> countUser(){
		return serviceUser.countUser();
	}
	
	
	
	



	
}
