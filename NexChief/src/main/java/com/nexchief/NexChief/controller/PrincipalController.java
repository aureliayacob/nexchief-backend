package com.nexchief.NexChief.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nexchief.NexChief.dto.PrincipalDto;
import com.nexchief.NexChief.repository.PersonRepository;
import com.nexchief.NexChief.repository.PrincipalRepository;
import com.nexchief.NexChief.response.Response;
import com.nexchief.NexChief.service.PrincipalService;

@RestController
public class PrincipalController {

	@Autowired
	PrincipalRepository repoPrincipal;

	@Autowired
	PersonRepository repoPerson;

	@Autowired
	PrincipalService servicePrincipal;

	@GetMapping (value="/api/principal/all",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getAllPrincipal(){
		return servicePrincipal.getAllPrincipal();}

	@GetMapping (value="/api/principal/{size}/{page}",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getAllPrincipalPage(@PathVariable("size") int size, @PathVariable("page") int page){
		return servicePrincipal.getAllPrincipalPage(size, page);}


	@GetMapping (value="/api/principal",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getPrincipalByPrincipalId(@RequestParam("id") String principalId){
		return servicePrincipal.getPrincipalByPrincipalId(principalId);
	}

	@GetMapping (value="/api/principal/search",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> searchPrincipal(@RequestParam("name") String name){
		return servicePrincipal.searchPrincipalByName(name);
	}

	@DeleteMapping (value="/api/principal/delete/{id}")
	public ResponseEntity<Response> deletePrincipal(@PathVariable(value="id") int id){
		return servicePrincipal.deletePrincipal(id);
	}

	@PutMapping (value="/api/principal/edit/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> editPrincipal(@PathVariable(value="id") int id, @RequestBody PrincipalDto payload){
		return servicePrincipal.editPrincipal(id, payload);
	}

	@PostMapping (value="/api/principal/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE  )
	public ResponseEntity<Response> addPrincipal(@RequestBody PrincipalDto payload){
		return servicePrincipal.addPrincipal(payload);
	}

	@GetMapping (value="/api/principals/count")
	public ResponseEntity<Response> countActivePrincipals(){
		return servicePrincipal.countActivePrincipal();

	}
	
	@GetMapping(value="/api/principal/{id}/havedistributor")
	public ResponseEntity<Response> isHaveDistributor(@PathVariable("id") int principalId){
		return servicePrincipal.isHaveDistributor(principalId);
	}
}
