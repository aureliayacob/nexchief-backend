package com.nexchief.NexChief.dto;

import java.sql.Timestamp;

public class NewUserDto {

	private boolean disableLogin;
	private String password;
	private Timestamp registrationDate;
	private String status;
	private String userId;
	private Timestamp userValidThru;
	private Integer distributorId;
	private int personId;
	private Integer principalId;
	private String name;
	private String email;
	private String phone;
	private String address;
	private String createdBy;
	private String updatedBy;

	public NewUserDto() {
		super();
	}

	public NewUserDto(boolean disableLogin, String password, Timestamp registrationDate, String status, String userId,
			Timestamp userValidThru, int distributorId, int principalId, String name, String email,
			String phone, String address, String createdBy, String updatedBy) {
		super();
		this.disableLogin = disableLogin;
		this.password = password;
		this.registrationDate = registrationDate;
		this.status = status;
		this.userId = userId;
		this.userValidThru = userValidThru;
		this.distributorId = distributorId;
		this.principalId = principalId;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	public boolean isDisableLogin() {
		return disableLogin;
	}

	public void setDisableLogin(boolean disableLogin) {
		this.disableLogin = disableLogin;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Timestamp getUserValidThru() {
		return userValidThru;
	}

	public void setUserValidThru(Timestamp userValidThru) {
		this.userValidThru = userValidThru;
	}

	public Integer getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(Integer distributorId) {
		this.distributorId = distributorId;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public Integer getPrincipalId() {
		return principalId;
	}

	public void setPrincipalId(int principalId) {
		this.principalId = principalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



}
