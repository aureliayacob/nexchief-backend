package com.nexchief.NexChief.dto;

import java.sql.Timestamp;
import java.util.Date;

public class PrincipalDto {
	private int id;
	private String principalId;
	private String city;
	private String fax;
	private String country;
	private Date licensedExpiryDate;
	private int personId;
	private String name;
	private String address; 
	private String phone;
	private String email;
	private Timestamp createdAt = new Timestamp(System.currentTimeMillis());
	private String createdBy;
	private Timestamp updatedAt = new Timestamp(System.currentTimeMillis());
	private String updatedBy;
	private Timestamp deletedAt;
	private String deletedBy;
	
	public PrincipalDto() {
		super();
	}
	

	public PrincipalDto(String principalId, String city, String fax, String country, Date licensedExpiryDate,
			String name, String address, String phone, String email, String createdBy, String updatedBy) {
		super();
		this.principalId = principalId;
		this.city = city;
		this.fax = fax;
		this.country = country;
		this.licensedExpiryDate = licensedExpiryDate;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}


	public PrincipalDto( String principalId, String city, String fax, String country, Date licensedExpiryDate,
			int personId, String name, String address, String phone, String email, Timestamp createdAt,
			String createdBy, Timestamp updatedAt, String updatedBy, Timestamp deletedAt, String deletedBy) {
		super();
		this.principalId = principalId;
		this.city = city;
		this.fax = fax;
		this.country = country;
		this.licensedExpiryDate = licensedExpiryDate;
		this.personId = personId;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
		this.deletedAt = deletedAt;
		this.deletedBy = deletedBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrincipalId() {
		return principalId;
	}

	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getLicensedExpiryDate() {
		return licensedExpiryDate;
	}

	public void setLicensedExpiryDate(Date licensedExpiryDate) {
		this.licensedExpiryDate = licensedExpiryDate;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}
	
	
	
	

}
