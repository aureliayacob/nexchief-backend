package com.nexchief.NexChief.dto;

import java.sql.Timestamp;

public class UserCreateUpdateDto {
	private int id;
	private String userId;
	private String password;
	private String status;
	private boolean disableLogin;
	private Timestamp registrationDate = new Timestamp(System.currentTimeMillis());;
	private Timestamp userValidThru;
	//person
	private int personId;
	private String name;
	private String address; 
	private String phone;
	private String email;
	private Timestamp createdAt = new Timestamp(System.currentTimeMillis());
	private String createdBy;
	private Timestamp updatedAt = new Timestamp(System.currentTimeMillis());
	private String updatedBy;
	private Timestamp deletedAt;
	private String deletedBy;
	////principal
	private Integer principalId;
	//distributor
	private Integer distributorId;
	
	public UserCreateUpdateDto() {
		super();
	}

	//create
	public UserCreateUpdateDto(String userId, String password, String status, boolean disableLogin,
			Timestamp userValidThru, String name, String address, String phone, String email, String createdBy,
			String updatedBy, Integer principalId, Integer distributorId) {
		super();
		this.userId = userId;
		this.password = password;
		this.status = status;
		this.disableLogin = disableLogin;
		this.userValidThru = userValidThru;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.principalId = principalId;
		this.distributorId = distributorId;
	}
	
	//update
		public UserCreateUpdateDto( String password, String status, boolean disableLogin,
				Timestamp userValidThru, String name, String address, String phone, String email,
				String updatedBy, Integer principalId, Integer distributorId) {
			super();
			this.password = password;
			this.status = status;
			this.disableLogin = disableLogin;
			this.userValidThru = userValidThru;
			this.name = name;
			this.address = address;
			this.phone = phone;
			this.email = email;
			this.updatedBy = updatedBy;
			this.principalId = principalId;
			this.distributorId = distributorId;
		}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isDisableLogin() {
		return disableLogin;
	}

	public void setDisableLogin(boolean disableLogin) {
		this.disableLogin = disableLogin;
	}

	public Timestamp getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Timestamp getUserValidThru() {
		return userValidThru;
	}

	public void setUserValidThru(Timestamp userValidThru) {
		this.userValidThru = userValidThru;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Integer getPrincipalId() {
		return principalId;
	}

	public void setPrincipalId(Integer principalId) {
		this.principalId = principalId;
	}

	public Integer getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(Integer distributorId) {
		this.distributorId = distributorId;
	}

	public int getId() {
		return id;
	}
	
	
	
	
}
