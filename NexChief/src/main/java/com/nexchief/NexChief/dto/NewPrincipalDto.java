package com.nexchief.NexChief.dto;

import java.time.LocalDate;

public class NewPrincipalDto {
	private String name;
	private String address;
	private String city;
	private String email;
	private String phone;
	private String createdBy;
	private String updatedBy;
	private String fax;
	private String country;
	private String principalId;
	private LocalDate licensedExpiryDate;


	public NewPrincipalDto() {
	}

	public NewPrincipalDto(String name, String address, String city, String email, String phone, String createdBy,
			String updatedBy, String fax, String country, String principalId, LocalDate licensedExpiryDate) {
		super();
		this.name = name;
		this.address = address;
		this.city = city;
		this.email = email;
		this.phone = phone;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.fax = fax;
		this.country = country;
		this.principalId = principalId;
		this.licensedExpiryDate = licensedExpiryDate;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public LocalDate getLicensedExpiryDate() {
		return licensedExpiryDate;
	}


	public void setLicensedExpiryDate(LocalDate licensedExpiryDate) {
		this.licensedExpiryDate = licensedExpiryDate;
	}




	public String getPrincipalId() {
		return principalId;
	}



	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}










}
