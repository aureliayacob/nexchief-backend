package com.nexchief.NexChief.dto;

public interface CustomAdmin {
	public interface AdminLogin{
	  	public Long getId();
    	public String getUsername();
    	public String getPassword();
	}

}
