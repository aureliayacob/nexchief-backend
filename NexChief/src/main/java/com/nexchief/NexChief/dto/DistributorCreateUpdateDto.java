package com.nexchief.NexChief.dto;

import java.sql.Timestamp;

public class DistributorCreateUpdateDto {
	
	private int id;
	private String distributorId;
	private String city;
	private String ownerFirstName;
	private String ownerLastName;
	private String fax;
	private String country;
	private String website;
	//person
	private int personId;
	private String name;
	private String address; 
	private String phone;
	private String email;
	private Timestamp createdAt = new Timestamp(System.currentTimeMillis());
	private String createdBy;
	private Timestamp updatedAt = new Timestamp(System.currentTimeMillis());
	private String updatedBy;
	private Timestamp deletedAt;
	private String deletedBy;
	//principal
	private Integer principalId;
	
	//edit
	public DistributorCreateUpdateDto(String city, String ownerFirstName, String ownerLastName, String fax,
			String country, String website, String name, String address, String phone, String email,
			Integer principalId) {
		super();
		this.city = city;
		this.ownerFirstName = ownerFirstName;
		this.ownerLastName = ownerLastName;
		this.fax = fax;
		this.country = country;
		this.website = website;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.principalId = principalId;
	}

	//add
	public DistributorCreateUpdateDto(String distributorId, String city, String ownerFirstName, String ownerLastName,
			String fax, String country, String website, String name, String address, String phone, String email,
			String createdBy, String updatedBy, Integer principalId) {
		super();
		this.distributorId = distributorId;
		this.city = city;
		this.ownerFirstName = ownerFirstName;
		this.ownerLastName = ownerLastName;
		this.fax = fax;
		this.country = country;
		this.website = website;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.principalId = principalId;
	}

	public DistributorCreateUpdateDto() {
		super();
	}

	public String getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getOwnerFirstName() {
		return ownerFirstName;
	}

	public void setOwnerFirstName(String ownerFirstName) {
		this.ownerFirstName = ownerFirstName;
	}

	public String getOwnerLastName() {
		return ownerLastName;
	}

	public void setOwnerLastName(String ownerLastName) {
		this.ownerLastName = ownerLastName;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public int getId() {
		return id;
	}

	public Integer getPrincipalId() {
		return principalId;
	}

	public void setPrincipalId(Integer principalId) {
		this.principalId = principalId;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	

	

}
