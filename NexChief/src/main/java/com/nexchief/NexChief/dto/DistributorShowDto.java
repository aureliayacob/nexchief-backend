package com.nexchief.NexChief.dto;

import com.nexchief.NexChief.customfields.CustomDistributor;
import com.nexchief.NexChief.customfields.CustomPrincipal;
import com.nexchief.NexChief.customfields.CustomDistributor.GetFullDataDistributor;

public class DistributorShowDto {
	private CustomDistributor.GetFullDataDistributor distributor;
	private CustomPrincipal.GetFullDataPrincipal principal;

	public DistributorShowDto(GetFullDataDistributor distributor,  CustomPrincipal.GetFullDataPrincipal principal) {
		super();
		this.distributor = distributor;
		this.principal = principal;
	}

	public CustomDistributor.GetFullDataDistributor getDistributor() {
		return distributor;
	}

	public void setDistributor(CustomDistributor.GetFullDataDistributor distributor) {
		this.distributor = distributor;
	}

	public CustomPrincipal.GetFullDataPrincipal getPrincipal() {
		return principal;
	}

	public void setPrincipal(CustomPrincipal.GetFullDataPrincipal principal) {
		this.principal = principal;
	}


}
