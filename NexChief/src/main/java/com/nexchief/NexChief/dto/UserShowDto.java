package com.nexchief.NexChief.dto;

import com.nexchief.NexChief.customfields.CustomDistributor;
import com.nexchief.NexChief.customfields.CustomPrincipal;
import com.nexchief.NexChief.customfields.CustomUser;
import com.nexchief.NexChief.customfields.CustomDistributor.GetFullDataDistributor;
import com.nexchief.NexChief.customfields.CustomPrincipal.GetFullDataPrincipal;
import com.nexchief.NexChief.customfields.CustomUser.GetFullDataUser;

public class UserShowDto {
	CustomUser.GetFullDataUser user;
	CustomPrincipal.GetFullDataPrincipal principal;
	CustomDistributor.GetFullDataDistributor distributor;
	
	public UserShowDto(GetFullDataUser user,  GetFullDataPrincipal principal, GetFullDataDistributor distributor) {
		super();
		this.user = user;
		this.principal = principal;
		this.distributor = distributor;
	}

	public CustomUser.GetFullDataUser getUser() {
		return user;
	}

	public void setUser(CustomUser.GetFullDataUser user) {
		this.user = user;
	}

	public GetFullDataPrincipal getPrincipal() {
		return principal;
	}

	public void setPrincipal(GetFullDataPrincipal principal) {
		this.principal = principal;
	}

	public GetFullDataDistributor getDistributor() {
		return distributor;
	}

	public void setDistributor(GetFullDataDistributor distributor) {
		this.distributor = distributor;
	}
	
	
	
	

}
