package com.nexchief.NexChief.dto;

public class EditPrincipalDto {
	private String name;
	private String city;
	private String country;
	private String fax;
	private String address;
	private String email;
	private String phone;
	private String licensedExpiryDate;
	private String updatedBy;
	
	public EditPrincipalDto(String name, String city, String country, String fax, String address, String email,
			String phone, String licensedExpiryDate, String updatedBy) {
		super();
		this.name = name;
		this.city = city;
		this.country = country;
		this.fax = fax;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.licensedExpiryDate = licensedExpiryDate;
		this.updatedBy = updatedBy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLicensedExpiryDate() {
		return licensedExpiryDate;
	}

	public void setLicensedExpiryDate(String licensedExpiryDate) {
		this.licensedExpiryDate = licensedExpiryDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}
