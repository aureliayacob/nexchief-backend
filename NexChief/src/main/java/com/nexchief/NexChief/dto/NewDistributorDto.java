package com.nexchief.NexChief.dto;

public class NewDistributorDto {
private String city;
private String country;
private String distributorId;
private String fax;
private String ownerFirstName;
private String ownerLastName;
private String website;
private int principalId;
private String name;
private String email;
private String phone;
private String address;
private String createdBy;
private String updatedBy;

public NewDistributorDto(String city, String country, String distributorId, String fax, String ownerFirstName,
		String ownerLastName, String website, int principalId, String name, String email, String phone, String address,
		String createdBy, String updatedBy) {
	super();
	this.city = city;
	this.country = country;
	this.distributorId = distributorId;
	this.fax = fax;
	this.ownerFirstName = ownerFirstName;
	this.ownerLastName = ownerLastName;
	this.website = website;
	this.principalId = principalId;
	this.name = name;
	this.email = email;
	this.phone = phone;
	this.address = address;
	this.createdBy = createdBy;
	this.updatedBy = updatedBy;
}

public NewDistributorDto() {
	super();
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public String getDistributorId() {
	return distributorId;
}

public void setDistributorId(String distributorId) {
	this.distributorId = distributorId;
}

public String getFax() {
	return fax;
}

public void setFax(String fax) {
	this.fax = fax;
}

public String getOwnerFirstName() {
	return ownerFirstName;
}

public void setOwnerFirstName(String ownerFirstName) {
	this.ownerFirstName = ownerFirstName;
}

public String getOwnerLastName() {
	return ownerLastName;
}

public void setOwnerLastName(String ownerLastName) {
	this.ownerLastName = ownerLastName;
}

public String getWebsite() {
	return website;
}

public void setWebsite(String website) {
	this.website = website;
}

public int getPrincipalId() {
	return principalId;
}

public void setPrincipalId(int principalId) {
	this.principalId = principalId;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getPhone() {
	return phone;
}

public void setPhone(String phone) {
	this.phone = phone;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}

public String getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}

	
}
