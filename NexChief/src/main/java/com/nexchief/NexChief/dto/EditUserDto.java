package com.nexchief.NexChief.dto;

import java.sql.Timestamp;

public class EditUserDto {
	private boolean disableLogin;
	private String password;
	private String status;
	private Timestamp userValidThru;
	private Integer distributorId;
	private Integer principalId;
	private String name;
	private String email;
	private String phone;
	private String address;
	private String updatedBy;

	public EditUserDto() {
		super();
	}
	
	public EditUserDto(boolean disableLogin, String password, String status, Timestamp userValidThru,
			Integer distributorId, int principalId, String name, String email, String phone, String address,
			Timestamp updatedAt, String updatedBy) {
		super();
		this.disableLogin = disableLogin;
		this.password = password;
		this.status = status;
		this.userValidThru = userValidThru;
		this.distributorId = distributorId;
		this.principalId = principalId;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.updatedBy = updatedBy;
	}

	public boolean isDisableLogin() {
		return disableLogin;
	}

	public void setDisableLogin(boolean disableLogin) {
		this.disableLogin = disableLogin;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUserValidThru() {
		return userValidThru;
	}

	public void setUserValidThru(Timestamp userValidThru) {
		this.userValidThru = userValidThru;
	}

	public Integer getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(Integer distributorId) {
		this.distributorId = distributorId;
	}

	public Integer getPrincipalId() {
		return principalId;
	}

	public void setPrincipalId(Integer principalId) {
		this.principalId = principalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	




}
