package com.nexchief.NexChief.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nexchief.NexChief.customfields.CustomPerson;
import com.nexchief.NexChief.entity.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long>{

	@Query(value="SELECT id, name, email, phone, address, created_at as createdAt, created_by as createdBy, updated_at as updatedAt, updated_by as updatedBy FROM person WHERE id=?", nativeQuery=true)
	Optional<CustomPerson.GetPerson> getPersonById(int id);
	

	@Modifying
	@Transactional
	@Query(value="INSERT INTO person (address, created_at, created_by, email, name, phone, updated_at, updated_by) "
			+ " VALUES (?1, current_timestamp(), ?2, ?3,?4, ?5, current_timestamp(), ?6)", nativeQuery = true)
	int addPerson(String address, String created_by, String email,String name, String phone, String updated_by);
	
	@Query(value="SELECT id from person order by id desc limit 1", nativeQuery = true)
	int getLastestPersonId();
	
	@Modifying
	@Transactional
	@Query(value="UPDATE person pr "
			+ "JOIN principal p "
			+ "ON pr.id = p.person_id "
			+ "AND p.id = ?1 "
			+ "SET pr.name = ?2, "
			+ "    pr.address = ?3,"
			+ "    pr.email = ?4,"
			+ "    pr.phone = ?5,"
			+ "    pr.updated_at = current_timestamp(),"
			+ "    pr.updated_by = ?6", nativeQuery = true)
	int updatePrincipal(int id, String name, String address, String email,
			String phone, String updatedBy);

	@Modifying
	@Transactional
	@Query(value="UPDATE person pr "
			+ "JOIN distributor d "
			+ "ON pr.id = d.person_id "
			+ "AND d.id = ?1 "
			+ "SET pr.name = ?2, "
			+ "    pr.address = ?3, "
			+ "    pr.email = ?4, "
			+ "    pr.phone = ?5, "
			+ "    pr.updated_at  = current_timestamp(), "
			+ "    pr.updated_by = ?6", nativeQuery = true)
	int updateDistributor(int id, String name, String address, String email,
			String phone, String updatedBy);


	@Modifying
	@Transactional
	@Query(value="UPDATE person pr "
			+ "JOIN user u "
			+ "ON pr.id = u.person_id "
			+ "AND u.id = ?1 "
			+ "SET pr.name = ?2, "
			+ "    pr.address = ?3, "
			+ "    pr.email = ?4, "
			+ "    pr.phone = ?5, "
			+ "    pr.updated_at  = current_timestamp(), "
			+ "    pr.updated_by = ?6", nativeQuery = true)
	int updateUser(int id, String name, String address, String email,
			String phone, String updatedBy);


	@Modifying
	@Transactional
	@Query(value="DELETE FROM person WHERE id=?", nativeQuery = true)
	int hardDeleteUserPerson(int personId);


}
