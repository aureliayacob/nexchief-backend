package com.nexchief.NexChief.repository;

import com.nexchief.NexChief.customfields.CustomAdmin;
import com.nexchief.NexChief.entity.Admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long>{
	
	@Query(value="SELECT id, password, username, person_id as personId from admin where username=?", nativeQuery=true)
	Optional<CustomAdmin.AdminLogin> findAdminByUsername(String username);
	
	@Query(value="SELECT id, username, person_id as personId from admin where username=?", nativeQuery=true)
	Optional<CustomAdmin.DataAdmin> getDataAdminByUsername(String username);
	

}
