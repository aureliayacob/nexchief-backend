package com.nexchief.NexChief.repository;

import java.sql.Timestamp;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.nexchief.NexChief.entity.DbRecord;

public interface DbRecordRespository extends JpaRepository<DbRecord, Integer> {
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO db_record (file_name, file_path, last_update) \r\n"
			+ "VALUES (?1, ?2, ?3)",
			nativeQuery=true)
	void recordBackup(String filename, String path, Timestamp date);
	
	@Query(value="SELECT id, file_name, file_path, last_update FROM db_record order by id desc limit 1",
			nativeQuery=true)
	DbRecord getLastestRecord();

}
