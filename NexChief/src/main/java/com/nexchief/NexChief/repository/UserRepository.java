package com.nexchief.NexChief.repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nexchief.NexChief.customfields.CustomUser.GetFullDataUser;
import com.nexchief.NexChief.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	@Query(value="SELECT u.id, "
			+ "       u.disable_login as disableLogin, "
			+ "       u.registration_date as registrationDate, "
			+ "       u.status, "
			+ "       u.user_id as userId, "
			+ "       u.user_valid_thru as userValidThru, "
			+ "       u.distributor_id as distributorId, "
			+ "       u.principal_id as principalId, "
			+ "		  u.person_id as personId,"
			+ "       pr.name, "
			+ "       pr.address, "
			+ "       pr.email, "
			+ "       pr.phone, "
			+ "       pr.created_at as createdAt, "
			+ "       pr.created_by as createdBy, "
			+ "       pr.updated_at as updatedAt, "
			+ "       pr.updated_by as updatedBy "
			+ "      FROM user u  "
			+ "    JOIN person pr ON u.person_id = pr.id "
			+ "    WHERE pr.deleted_at IS NULL", nativeQuery=true)
	List<GetFullDataUser> getAllUserPage(Pageable page);



	@Query(value="SELECT u.id, "
			+ "       u.disable_login as disableLogin, "
			+ "       u.registration_date as registrationDate, "
			+ "       u.status, "
			+ "       u.user_id as userId, "
			+ "       u.user_valid_thru as userValidThru, "
			+ "       u.distributor_id as distributorId, "
			+ "       u.principal_id as principalId, "
			+ "		  u.person_id as personId,"
			+ "       pr.name, "
			+ "       pr.address, "
			+ "       pr.email, "
			+ "       pr.phone, "
			+ "       pr.created_at as createdAt, "
			+ "       pr.created_by as createdBy, "
			+ "       pr.updated_at as updatedAt, "
			+ "       pr.updated_by as updatedBy "
			+ "      FROM user u  "
			+ "    JOIN person pr ON u.person_id = pr.id "
			+ "    WHERE pr.deleted_at IS NULL", nativeQuery=true)
	List<GetFullDataUser> getAllUser();

	// GET user by id (int)
	@Query(value="SELECT u.id, "
			+ "       u.disable_login as disableLogin, "
			+ "       u.registration_date as registrationDate, "
			+ "       u.status, "
			+ "       u.user_id as userId, "
			+ "       u.user_valid_thru as userValidThru, "
			+ "       u.distributor_id as distributorId, "
			+ "       u.principal_id as principalId, "
			+ "		  u.person_id as personId, "
			+ "       pr.name,  "
			+ "       pr.address, "
			+ "       pr.email, "
			+ "       pr.phone, "
			+ "       pr.created_at as createdAt, "
			+ "       pr.created_by as createdBy, "
			+ "       pr.updated_at as updatedAt, "
			+ "       pr.updated_by as updatedBy"
			+ "    FROM user u  "
			+ "    JOIN person pr ON u.person_id = pr.id "
			+ "    WHERE pr.deleted_at IS NULL"
			+ "	   AND u.id=?", nativeQuery=true)
	Optional<GetFullDataUser> getUserById(int id);

	//GET user by id (String)
	@Query(value="SELECT u.id, "
			+ "       u.disable_login as disableLogin, "
			+ "       u.registration_date as registrationDate, "
			+ "       u.status, "
			+ "       u.user_id as userId, "
			+ "       u.user_valid_thru as userValidThru, "
			+ "       u.distributor_id as distributorId, "
			+ "       u.principal_id as principalId, "
			+ "		  u.person_id as personId, "
			+ "       pr.name,  "
			+ "       pr.address, "
			+ "       pr.email, "
			+ "       pr.phone, "
			+ "       pr.created_at as createdAt, "
			+ "       pr.created_by as createdBy, "
			+ "       pr.updated_at as updatedAt, "
			+ "       pr.updated_by as updatedByuser "
			+ "    FROM user u  "
			+ "    JOIN person pr ON u.person_id = pr.id "
			+ "    WHERE pr.deleted_at IS NULL"
			+ "    AND u.user_id=?", nativeQuery=true)
	List<GetFullDataUser> getUserByUserId(String id);

//
//	@Modifying
//	@Transactional
//	@Query(value="INSERT INTO user (disable_login, password, registration_date, status, user_id, user_valid_thru, distributor_id, person_id, principal_id) "
//			+ "VALUES ( ?1, ?2, CURRENT_TIMESTAMP(), ?3, ?4, ?5, ?6, ?7, ?8)", nativeQuery = true)
//	int addUser(boolean disableLogin, String password, String status, String user_id, LocalDateTime user_valid_thru, int distributorId, int personId, int principalId);

	@Modifying
	@Transactional
	@Query(value="UPDATE person pr JOIN user u ON pr.id = u.person_id AND u.id=? SET deleted_at=current_timestamp()", nativeQuery = true)
	int deleteUser(int id);

	@Modifying
	@Transactional
	@Query(value="DELETE FROM user WHERE id = ?", nativeQuery = true)
	int hardDeleteUser(int id);

	@Query(value="SELECT u.id, "
			+ "    u.disable_login as disableLogin, "
			+ "    u.registration_date as registrationDate, "
			+ "    u.status, "
			+ "    u.user_id as userId, "
			+ "    u.user_valid_thru as userValidThru, "
			+ "    u.distributor_id as distributorId, "
			+ "    u.principal_id as principalId, "
			+ "    pr.name,  "
			+ "    pr.address, "
			+ "    pr.email, "
			+ "    pr.phone, "
			+ "    pr.created_at as createdAt, "
			+ "    pr.created_by as createdBy, "
			+ "    pr.updated_at as updatedAt, "
			+ "    pr.updated_by as updatedByuser "
			+ "    FROM user u  "
			+ "    JOIN person pr ON u.person_id = pr.id "
			+ "    WHERE pr.deleted_at IS NULL"
			+ "	   AND (pr.name LIKE %:keyword% "
			+ "    OR u.user_id LIKE %:keyword%)"
			+ "    ORDER BY pr.name", nativeQuery = true)
	List<GetFullDataUser> searchUser (String keyword);

	@Modifying
	@Transactional
	@Query(value="UPDATE user "
			+ "SET "
			+ "disable_login = ?1, "
			+ "status = ?2,"
			+ "user_valid_thru = ?3, "
			+ "distributor_id = ?4, "
			+ "principal_id =?5 "
			+ "WHERE id =?6", nativeQuery=true)
	int updateUser(boolean disableLogin, String status, Timestamp userValidThru, Integer distributorId, int principalId, int id);

	@Query(value="SELECT count(user.id)\r\n"
			+ "FROM user\r\n"
			+ "JOIN person\r\n"
			+ "ON user.person_id = person.id\r\n"
			+ "WHERE person.deleted_at IS NULL",nativeQuery = true)
	int countUser();

	@Modifying
	@Transactional
	@Query(value="INSERT INTO user (disable_login, password, registration_date, status, user_id, user_valid_thru, distributor_id, person_id, principal_id) "
			+ "VALUES (?1, ?2, CURRENT_TIMESTAMP(), ?3, ?4, ?5, ?6,?7,?8)", nativeQuery = true)
	int insertUser(boolean disableLogin, String password, String status, String userId, Timestamp userValidThru, Integer distributorId, int personId, int principalId);

}

