package com.nexchief.NexChief.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.nexchief.NexChief.customfields.CustomDistributor.GetFullDataDistributor;
import com.nexchief.NexChief.entity.Distributor;

public interface DistributorRepository extends JpaRepository<Distributor, Long>{

	@Query(value="SELECT d.id, "
			+ "	  d.distributor_id as distributorId,"
			+ "	  d.principal_id as principalId, "
			+ "   pr.name, "
			+ "   d.city, "
			+ "   d.country, "
			+ "   d.fax, "
			+ "   d.owner_first_name as ownerFirstName, "
			+ "   d.owner_last_name as ownerLastName, "
			+ "   d.website, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   d.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy "
			+ "   FROM distributor d  "
			+ "   JOIN person pr ON d.person_id = pr.id "
			+ "   WHERE pr.deleted_at IS NULL ", nativeQuery=true)
	List<GetFullDataDistributor> getAllDistributorsPage(Pageable pageable);

	@Query(value="SELECT d.id, "
			+ "	  d.distributor_id as distributorId,"
			+ "	  d.principal_id as principalId, "
			+ "   pr.name, "
			+ "   d.city, "
			+ "   d.country, "
			+ "   d.fax, "
			+ "   d.owner_first_name as ownerFirstName, "
			+ "   d.owner_last_name as ownerLastName, "
			+ "   d.website, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   d.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy "
			+ "   FROM distributor d  "
			+ "   JOIN person pr ON d.person_id = pr.id "
			+ "   WHERE pr.deleted_at IS NULL ", nativeQuery=true)
	List<GetFullDataDistributor> getAllDistributors();

	@Query(value="SELECT d.id, "
			+ "	  d.distributor_id as distributorId,"
			+ "	  d.principal_id as principalId, "
			+ "   pr.name, "
			+ "   d.city, "
			+ "   d.country, "
			+ "   d.fax, "
			+ "   d.owner_first_name as ownerFirstName, "
			+ "   d.owner_last_name as ownerLastName, "
			+ "   d.website, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   d.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy "
			+ "   FROM distributor d  "
			+ "   JOIN person pr ON d.person_id = pr.id "
			+ "   WHERE pr.deleted_at IS NULL "
			+ "   AND d.principal_id =?", nativeQuery=true)
	List<GetFullDataDistributor> getAllDistributorsPerPrincipal(int id);


	@Query(value="SELECT d.id, "
			+ "	  d.distributor_id as distributorId, "
			+ "	  d.principal_id as principalId, "
			+ "   pr.name, "
			+ "   d.city, "
			+ "   d.country, "
			+ "   d.fax, "
			+ "   d.owner_first_name as ownerFirstName, "
			+ "   d.owner_last_name as ownerLastName, "
			+ "   d.website, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   d.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy "
			+ "   FROM distributor d  "
			+ "   JOIN person pr ON d.person_id = pr.id "
			+ "   WHERE pr.deleted_at IS NULL"
			+ "   AND d.distributor_id = ?", 
			nativeQuery=true)
	Optional<GetFullDataDistributor> getDistributorByDistributorId(String distributorId);


	@Query(value="SELECT d.id, "
			+ "	  d.city,"
			+ "	  d.country, "
			+ "   d.distributor_id, "
			+ "   d.fax, "
			+ "   d.owner_first_name, "
			+ "   d.owner_last_name, "
			+ "   d.website, "
			+ "   d.person_id, "
			+ "   d.principal_id "
			+ "   FROM distributor d  "
			+ "   WHERE d.id = ?", 
			nativeQuery=true)
	Optional<Distributor> getDistributorById(int id);

	@Query(value="SELECT d.id, "
			+ "	  d.distributor_id as distributorId, "
			+ "	  d.principal_id as principalId, "
			+ "   pr.name, "
			+ "   d.city, "
			+ "   d.country, "
			+ "   d.fax, "
			+ "   d.owner_first_name as ownerFirstName, "
			+ "   d.owner_last_name as ownerLastName, "
			+ "   d.website, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   d.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy "
			+ "   FROM distributor d  "
			+ "   JOIN person pr ON d.person_id = pr.id "
			+ "   WHERE d.id = ?", 
			nativeQuery=true)
	Optional<GetFullDataDistributor> getCustomDistributorById(int id);


	@Modifying
	@Transactional
	@Query(value="UPDATE person pr "
			+ "	  JOIN distributor d "
			+ "   ON pr.id = d.person_id AND d.id=? "
			+ "   SET deleted_at=current_timestamp(), "
			+ "   deleted_by = 'Admin'", 
			nativeQuery = true)
	int deleteDistributor(int id);

	@Query(value="SELECT d.id, "
			+ "	  d.distributor_id as distributorId,"
			+ "	  d.principal_id as principalId, "
			+ "   pr.name, "
			+ "   d.city, "
			+ "   d.country, "
			+ "   d.fax, "
			+ "   d.owner_first_name as ownerFirstName, "
			+ "   d.owner_last_name as ownerLastName, "
			+ "   d.website, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   d.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy "
			+ "   FROM distributor d  "
			+ "   JOIN person pr ON d.person_id = pr.id "
			+ "   WHERE pr.deleted_at IS NULL "
			+ "   AND (pr.name LIKE %:keyword% "
			+ "   OR d.distributor_id LIKE %:keyword%)"
			+ "   ORDER BY pr.name", 
			nativeQuery = true)
	List<GetFullDataDistributor> searchByName(String keyword);

	@Modifying
	@Transactional
	@Query(value="UPDATE distributor d "
			+ "	  SET city = ?1, "
			+ "   country = ?2, "
			+ "   fax = ?3, "
			+ "   owner_first_name = ?4, "
			+ "   owner_last_name = ?5, "
			+ "   website = ?6, "
			+ "   principal_id=?7 "
			+ "WHERE id=?8", nativeQuery = true)
	int updateDistributor (String city, String country, String fax, String firstName,
			String lastName, String website, int principalId, int id);

	@Query(value="SELECT count(distributor.id)\r\n"
			+ "FROM distributor\r\n"
			+ "JOIN person\r\n"
			+ "ON distributor.person_id = person.id\r\n"
			+ "WHERE person.deleted_at IS NULL",nativeQuery = true)
	int countDistributor();


	@Modifying
	@Transactional
	@Query(value="INSERT INTO distributor "
			+ "(city, country, distributor_id, fax , owner_first_name, owner_last_name, website, person_id, principal_id) "
			+ "VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)", nativeQuery = true)
	int insertDistributor(String city, String country, String distributor_id, String fax, String firstName,
			String lastName, String website, int personlId, int principalId);


}


