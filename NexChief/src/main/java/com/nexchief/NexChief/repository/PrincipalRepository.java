package com.nexchief.NexChief.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.nexchief.NexChief.customfields.CustomPrincipal;
import com.nexchief.NexChief.customfields.CustomPrincipal.GetFullDataPrincipal;
import com.nexchief.NexChief.entity.Principal;

public interface PrincipalRepository extends JpaRepository<Principal, Long>{


	@Query(value="SELECT p.id, "
			+ "	  p.principal_id as principalId, "
			+ "   pr.name, "
			+ "   p.city, "
			+ "   p.country, "
			+ "   p.fax, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   p.licensed_expiry_date as licensedExpiryDate, "
			+ "   p.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy, "
			+ "   pr.deleted_at as deletedAt, "
			+ "   pr.deleted_by as deletedBy "
			+ "   FROM principal p "
			+ "   JOIN person pr ON p.person_id = pr.id"
			+ "	  AND pr.deleted_at IS NULL", 
			nativeQuery=true)
	List<GetFullDataPrincipal> getAllPrincipalsPage(Pageable pageable);


	@Query(value="SELECT p.id, "
			+ "	  p.principal_id as principalId, "
			+ "   pr.name, "
			+ "   p.city, "
			+ "   p.country, "
			+ "   p.fax, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   p.licensed_expiry_date as licensedExpiryDate, "
			+ "   p.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy, "
			+ "   pr.deleted_at as deletedAt, "
			+ "   pr.deleted_by as deletedBy "
			+ "   FROM principal p "
			+ "   JOIN person pr ON p.person_id = pr.id"
			+ "	  AND pr.deleted_at IS NULL"
			+ "   AND p.licensed_expiry_date > NOW()", 
			nativeQuery=true)
	List<GetFullDataPrincipal> getAllPrincipals();

	@Query(value="SELECT p.id, "
			+ "	  p.principal_id as principalId, "
			+ "   pr.name, "
			+ "   p.city, "
			+ "   p.country, "
			+ "   p.fax, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   p.licensed_expiry_date as licensedExpiryDate, "
			+ "   p.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy, "
			+ "   pr.deleted_at as deletedAt, "
			+ "   pr.deleted_by as deletedBy "
			+ "   FROM principal p "
			+ "   JOIN person pr ON p.person_id = pr.id  "
			+ "   WHERE pr.name LIKE %:name% "
			+ "	  AND pr.deleted_at IS NULL"
			+ "   ORDER BY pr.name", nativeQuery = true)
	List<CustomPrincipal.GetFullDataPrincipal> searchPrincipalByName(String name);

	@Query(value="SELECT p.id, "
			+ "	  p.principal_id as principalId, "
			+ "   pr.name, "
			+ "   p.city, "
			+ "   p.country, "
			+ "   p.fax, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   p.licensed_expiry_date as licensedExpiryDate, "
			+ "   p.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy "
			+ "   FROM principal p "
			+ "   JOIN person pr ON p.person_id = pr.id"
			+ "	  AND pr.deleted_at IS NULL"
			+ "   WHERE p.principal_id=?", 
			nativeQuery=true)
	Optional<GetFullDataPrincipal> getPrincipalByPrincipalId(String id);

	@Query(value="SELECT p.id, "
			+ "   p.city, "
			+ "   p.country, "
			+ "   p.fax, "
			+ "   p.licensed_expiry_date, "
			+ "	  p.principal_id, "
			+ "   p.person_id "
			+ "   FROM principal p "
			+ "   WHERE p.id=?",
			nativeQuery=true)
	Optional<Principal> getPrincipalObjById(int id);

	@Query(value="SELECT p.id, "
			+ "   p.principal_id as principalId, "
			+ "   pr.name, "
			+ "   p.city, "
			+ "   p.country, "
			+ "   p.fax, "
			+ "   pr.address, "
			+ "   pr.email, "
			+ "   pr.phone, "
			+ "   p.licensed_expiry_date as licensedExpiryDate, "
			+ "   p.person_id as personId, "
			+ "   pr.created_at as createdAt, "
			+ "   pr.created_by as createdBy, "
			+ "   pr.updated_at as updatedAt, "
			+ "   pr.updated_by as updatedBy, "
			+ "   pr.deleted_at as deletedAt, "
			+ "   pr.deleted_by as deletedBy "
			+ "   FROM principal p "
			+ "   JOIN person pr ON p.person_id = pr.id  "
			+ "   WHERE p.id=?",
			nativeQuery=true)
	Optional<GetFullDataPrincipal> getPrincipalById(int id);


	@Transactional
	@Modifying
	@Query(value="INSERT INTO principal (city, country, fax, licensed_expiry_date, principal_id, person_id) "
			+ "   VALUES (?1, ?2, ?3, ?4, ?5, ?6)", nativeQuery=true)
	int addPrincipal(String city, String country, String fax, Date expiryDate, String principalId, int personId);

	@Modifying
	@Transactional
	@Query(value="UPDATE person pr "
			+ "	  JOIN principal p "
			+ "	  ON pr.id = p.person_id "
			+ "	  AND p.id = ?"
			+ "	  SET deleted_at= current_timestamp()", nativeQuery = true)
	int deletePrincipal(int id);

	@Modifying 
	@Transactional
	@Query(value="UPDATE principal "
			+ "   SET city = ?1, "
			+ "   country = ?2, "
			+ "   fax = ?3, "
			+ "   licensed_expiry_date = ?4 "
			+ "   WHERE id = ?5", nativeQuery=true)
	int updatePrincipal(String city, String country, String fax, Date licensedExpiryDate, int id );

	@Query(value="SELECT count(principal.id)\r\n"
			+ "FROM principal\r\n"
			+ "JOIN person\r\n"
			+ "ON principal.person_id = person.id\r\n"
			+ "WHERE person.deleted_at IS NULL "
			, nativeQuery=true)
	int countPrincipals();

	@Query(value="SELECT count(id) FROM distributor\r\n"
			+ "WHERE principal_id = ?", nativeQuery = true)
	int countDistributor(int id);
}
