package com.nexchief.NexChief.repository;

public interface CustomAdmin {
	public interface AdminLogin{
	  	public Long getId();
    	public String getUsername();
    	public String getPassword();
	}
	public interface DataAdmin{
	  	public Long getId();
    	public String getUsername();
	}

}
