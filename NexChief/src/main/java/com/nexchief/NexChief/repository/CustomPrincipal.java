package com.nexchief.NexChief.repository;

import java.sql.Timestamp;

public interface CustomPrincipal {

	public interface GetAllPrincipal{
		public int getId();
		public String getCity();
		public String getCountry();
		public String getFax();
		public Timestamp getLicensedExpiryDate();
		public String getPrincipalId();
		public int getPersonId();
	}

	public interface GetFullDataPrincipal{
		public int getId();
		public String getprincipalId();
		public String getName();
		public String getCity();
		public String getCountry();
		public String getFax();
		public String getAddress();
		public String getEmail();
		public String getPhone();
		public Timestamp getLicensedExpiryDate();
		public int getPersonId();
		public Timestamp getCreatedAt();
		public String getCreatedBy();
		public Timestamp getUpdatedAt();
		public String getUpdatedBy();
		public Timestamp getDeletedAt();
		public String getDeletedBy();
	}



}
