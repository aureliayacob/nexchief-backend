package com.nexchief.NexChief.customfields;

import java.sql.Timestamp;
public interface CustomUser {

	public interface GetFullDataUser{
		public int getId();
		public boolean getDisableLogin();
		public Timestamp getRegistrationDate();
		public String getStatus();
		public String getUserId();
		public Timestamp getUserValidThru();
		public Integer getDistributorId();
		public Integer getPrincipalId();
		public String getName(); 
		public String getAddress();
		public String getEmail();
		public String getPhone();
		public Timestamp getCreatedAt();
		public String getCreatedBy();
		public Timestamp getUpdatedAt();
		public String getUpdatedBy();
		public Timestamp getDeletedAt();
		public String getDeletedBy();
		public Integer getPersonId();
		public String getPassword();

	}


}
