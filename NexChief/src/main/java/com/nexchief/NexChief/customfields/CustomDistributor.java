package com.nexchief.NexChief.customfields;

import java.sql.Timestamp;

public interface CustomDistributor {

	public interface GetFullDataDistributor{
		public int getId(); 
		public String getDistributorId();
		public int getPrincipalId(); 
		public String getName(); 
		public String getCity();
		public String getCountry(); 
		public String getFax();
		public String getOwnerFirstName();
		public String getownerLastName();
		public String getWebsite();
		public String getAddress();
		public String getEmail();
		public String getPhone();
		public Timestamp getCreatedAt();
		public String getCreatedBy();
		public Timestamp getUpdatedAt();
		public String getUpdatedBy();
		public Timestamp getDeletedAt();
		public String getDeletedBy();
	}

}
