package com.nexchief.NexChief.customfields;

import java.sql.Timestamp;

public interface CustomPerson {

	public interface GetPerson{
		public int getId();
		public String getName();
		public String getEmail();
		public String getPhone();
		public String getAddress();
		public Timestamp getCreatedAt();
		public String getCreatedBy();
		public Timestamp getUpdatedAt();
		public String getUpdatedBy();

	}
}
