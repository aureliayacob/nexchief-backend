package com.nexchief.NexChief.customfields;

import java.sql.Timestamp;
import java.util.Date;

public interface CustomPrincipal {
	
	public interface GetFullDataPrincipal{
		public int getId();
		public String getprincipalId();
		public String getName();
		public String getCity();
		public String getCountry();
		public String getFax();
		public String getAddress();
		public String getEmail();
		public String getPhone();
		public Date getLicensedExpiryDate();
		public Integer getPersonId();
		public Timestamp getCreatedAt();
		public String getCreatedBy();
		public Timestamp getUpdatedAt();
		public String getUpdatedBy();
		public Timestamp getDeletedAt();
		public String getDeletedBy();
	}



}
