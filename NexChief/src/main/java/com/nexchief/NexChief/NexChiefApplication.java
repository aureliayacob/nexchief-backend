package com.nexchief.NexChief;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NexChiefApplication {

	public static void main(String[] args) {
		SpringApplication.run(NexChiefApplication.class, args);
	}

}
