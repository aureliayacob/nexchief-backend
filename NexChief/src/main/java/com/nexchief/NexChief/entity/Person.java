package com.nexchief.NexChief.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Person {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Column(name="name")
	private String name;

	@NotNull
	@Column(name ="address")
	private String address; 

	@NotNull
	@Column(name = "phone")
	private String phone;
	
	@NotNull
	@Column(name="email")
	private String email;

	@NotNull
	@Column(name = "created_at")
	private Timestamp createdAt = new Timestamp(System.currentTimeMillis());

	@Column(name = "created_by")
	private String createdBy;

	@NotNull
	@Column(name = "updated_at")
	private Timestamp updatedAt = new Timestamp(System.currentTimeMillis());

	@Column(name = "updated_by")
	private String updatedBy;
	
	
	@Column(name = "deleted_at")
	private Timestamp deletedAt;


	@Column(name = "deleted_by")
	private String deletedBy;
	

	public Person() {
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public int getId() {
		return id;
	}







	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", address=" + address + ", phone=" + phone + ", email=" + email
				+ ", createdAt=" + createdAt + ", createdBy=" + createdBy + ", updatedAt=" + updatedAt + ", updatedBy="
				+ updatedBy + ", deletedAt=" + deletedAt + "]";
	}



	public Person(@NotNull String name, @NotNull String address, @NotNull String phone, String email, String createdBy, String updatedBy) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}







}
