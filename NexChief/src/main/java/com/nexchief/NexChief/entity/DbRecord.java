package com.nexchief.NexChief.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DbRecord {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;
	
	private Timestamp lastUpdate = new Timestamp(System.currentTimeMillis());
	
	private String fileName;
	
	private String filePath;

	
	
	public DbRecord() {
		super();
	}



	public DbRecord(String fileName, String filePath) {
		super();
		this.fileName = fileName;
		this.filePath = filePath;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public Timestamp getLastUpdate() {
		return lastUpdate;
	}


	public String getFileName() {
		return fileName;
	}



	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	public String getFilePath() {
		return filePath;
	}



	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}



	@Override
	public String toString() {
		return "DbRecord [id=" + id + ", lastUpdate=" + lastUpdate + ", fileName=" + fileName + ", filePath=" + filePath
				+ "]";
	}
	
	
	

}
