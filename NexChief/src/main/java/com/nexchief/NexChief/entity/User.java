package com.nexchief.NexChief.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.sun.istack.NotNull;


@Entity
public class User {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Column (name="user_id", unique = true)
	private String userId;
	
	@NotNull
	@Column (name="password")
	private String password;
	
	private String status;
	
	@Column (name="disable_login")
	private boolean disableLogin;
	
	@Column (name="registration_date")
	private Timestamp registrationDate = new Timestamp(System.currentTimeMillis());;
	
	@Column (name="user_valid_thru")
	private Timestamp userValidThru;
	
	@OneToOne
	@JoinColumn(name="person_id", referencedColumnName="id")
	private Person person;
	
	@ManyToOne
	@JoinColumn(name="distributor_id", referencedColumnName="id")
	private Distributor distributor;
	
	@javax.validation.constraints.NotNull
	@ManyToOne
	@JoinColumn(name="principal_id", referencedColumnName="id")
	private Principal principal;

	public User() {
		super();
	}

	public User(String userId, String password, String status, boolean disableLogin,
			Timestamp userValidThru, Person person, Distributor distributor, Principal principal) {
		super();
		this.userId = userId;
		this.password = password;
		this.status = status;
		this.disableLogin = disableLogin;
		this.userValidThru = userValidThru;
		this.person = person;
		this.distributor = distributor;
		this.principal = principal;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isDisableLogin() {
		return disableLogin;
	}

	public void setDisableLogin(boolean disableLogin) {
		this.disableLogin = disableLogin;
	}

	public Timestamp isRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Timestamp isUserValidThru() {
		return userValidThru;
	}

	public void setUserValidThru(Timestamp userValidThru) {
		this.userValidThru = userValidThru;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Distributor getDistributor() {
		return distributor;
	}

	public void setDistributor(Distributor distributor) {
		this.distributor = distributor;
	}

	public Principal getPrincipal() {
		return principal;
	}

	public void setPrincipal(Principal principal) {
		this.principal = principal;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userId=" + userId + ", password=" + password + ", status=" + status
				+ ", disableLogin=" + disableLogin + ", registrationDate=" + registrationDate + ", userValidThru="
				+ userValidThru + ", person=" + person + ", distributor=" + distributor + ", principal=" + principal
				+ "]";
	}
	
	
	
	
	
	
	
	
	


}
