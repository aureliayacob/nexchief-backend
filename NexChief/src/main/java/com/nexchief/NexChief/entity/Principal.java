package com.nexchief.NexChief.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Principal {

	@Id
	@GeneratedValue (strategy =  GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Column (name="principal_id", unique = true)
	private String principalId;

	@NotNull
	@Column (name="city")
	private String city;
	private String fax;
	private String country;

	@Column (name="licensed_expiry_date")
	private Date licensedExpiryDate;

	@OneToOne
	@JoinColumn(name = "person_id", referencedColumnName="id")
	private Person person;

	public Principal() {
	}


	public String getPrincipalId() {
		return principalId;
	}

	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getLicensedExpiryDate() {
		return licensedExpiryDate;
	}

	public void setLicensedExpiryDate(Date licensedExpiryDate) {
		this.licensedExpiryDate = licensedExpiryDate;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Principal [id=" + id + ", principalId=" + principalId + ", city=" + city + ", fax=" + fax + ", country="
				+ country + ", licensedExpiryDate=" + licensedExpiryDate + ", person=" + person + "]";
	}


	public Principal(@NotNull String principalId, @NotNull String city, String fax, String country,
			Date licensedExpiryDate, Person person) {
		super();
		this.principalId = principalId;
		this.city = city;
		this.fax = fax;
		this.country = country;
		this.licensedExpiryDate = licensedExpiryDate;
		this.person = person;
		
	}




}
