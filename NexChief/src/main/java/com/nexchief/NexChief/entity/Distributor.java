package com.nexchief.NexChief.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;


@Entity
public class Distributor {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Column (name="distributor_id", unique = true)
	private String distributorId;

	@NotNull
	@Column (name="city")
	private String city;

	@NotNull
	@Column (name="owner_first_name")
	private String ownerFirstName;

	@NotNull
	@Column (name="owner_last_name")
	private String ownerLastName;

	private String fax;
	private String country;
	private String website;

	@OneToOne
	@JoinColumn(name = "person_id", referencedColumnName="id")
	private Person person;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "principal_id", referencedColumnName="id")
	Principal principal;

	public Distributor() {
	}

	public Distributor(String distributorId, String city, String ownerFirstName, String ownerLastName, String fax,
			String country, String website, Person person, Principal principal) {
		super();
		this.distributorId = distributorId;
		this.city = city;
		this.ownerFirstName = ownerFirstName;
		this.ownerLastName = ownerLastName;
		this.fax = fax;
		this.country = country;
		this.website = website;
		this.person = person;
		this.principal = principal;
	}

	public String getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getOwnerFirstName() {
		return ownerFirstName;
	}

	public void setOwnerFirstName(String ownerFirstName) {
		this.ownerFirstName = ownerFirstName;
	}

	public String getOwnerLastName() {
		return ownerLastName;
	}

	public void setOwnerLastName(String ownerLastName) {
		this.ownerLastName = ownerLastName;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Principal getPrincipal() {
		return principal;
	}

	public void setPrincipal(Principal principal) {
		this.principal = principal;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Distributor [id=" + id + ", distributorId=" + distributorId + ", city=" + city + ", ownerFirstName="
				+ ownerFirstName + ", ownerLastName=" + ownerLastName + ", fax=" + fax + ", country=" + country
				+ ", website=" + website + ", person=" + person + ", principal=" + principal + "]";
	}
	
	




}
